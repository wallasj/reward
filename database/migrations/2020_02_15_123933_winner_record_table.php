<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WinnerRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winner_record', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '中奖记录';
            $table->bigIncrements('id');
            $table->integer('activiteId')->comment('活动ID')->index();
            $table->integer('userId')->comment('用户ID')->index();
            $table->integer('deptId')->nullable()->comment('部门ID');
            $table->integer('prizeId')->comment('奖品ID')->index();
            $table->string('name', 50)->comment('奖品名称')->index();
            $table->tinyInteger('level')->comment('奖品等级')->default(0);
            $table->tinyInteger('status')->comment('是否有效 0无效 1有效')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('winner_record');
    }
}
