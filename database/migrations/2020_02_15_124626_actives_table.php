<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actives', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '活动表';
            $table->bigIncrements('id');
            $table->string('activeTitle')->comment('活动名称')->index();
            $table->string('description')->comment('抽奖规则')->index();
            $table->text('banners')->nullable()->comment('活动宣传图片');
            $table->tinyInteger('status')->comment('活动状态 0未开启 1已开启')->default(0);
            $table->integer('persons')->comment('参与人数')->default(0);
            $table->integer('actual_persons')->comment('实际参与人数')->default(0);
            $table->smallInteger('chance')->comment('抽奖次数')->default(2);
            $table->timestamp('beginTime')->nullable()->comment('开始时间');
            $table->timestamp('endTime')->nullable()->comment('结束时间');
            $table->string('timeDays')->comment('指定抽奖日期');
            $table->tinyInteger('timeHour')->comment('指定时间');
            $table->tinyInteger('timeMinutes')->comment('指定秒');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('actives');
    }
}
