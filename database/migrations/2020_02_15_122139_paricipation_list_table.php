<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ParicipationListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paricipation_list', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '参与名单';
            $table->bigIncrements('id');
            $table->integer('activeId')->comment('活动ID')->index();
            $table->integer('userId')->comment('用户ID')->index();
            $table->integer('prizeId')->default(0)->comment('奖品ID')->index();
            $table->tinyInteger('isReward')->comment('是否中奖 0否 1是')->default(0);
            $table->tinyInteger('chance')->comment('抽奖次数')->default(1);
            $table->tinyInteger('status')->comment('是否有效 0无效 1有效')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('paricipation_list');
    }
}
