<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivesPrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actives_prizes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '活动奖品表';
            $table->bigIncrements('id');
            $table->integer('activiteId')->comment('活动ID')->index();
            $table->integer('prizeId')->comment('奖品ID')->index();
            $table->tinyInteger('quantity')->comment('奖品数量')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('actives_prizes');
    }
}
