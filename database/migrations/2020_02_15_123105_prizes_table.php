<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->comment = '奖品表';
            $table->bigIncrements('id');
            $table->string('name', 50)->comment('奖品名称')->unique();
            $table->text('imgs')->nullable()->comment('奖品图片');
            $table->string('description', 150)->nullable()->comment('奖品描述');
            $table->tinyInteger('level')->comment('奖品等级');
            $table->decimal('price', 8, 2)->comment('奖品金额')->default(0);
            $table->smallInteger('stock')->comment('库存')->default(0);
            $table->tinyInteger('status')->comment('是否有效 0下架 1上架')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Illuminate\Support\Facades\Schema::dropIfExists('prizes');
    }
}
