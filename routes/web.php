<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'prizes'], function () use ($router) {
    //奖品列表
    $router->get('index', ['uses' => 'PrizesController@index']);

    //奖品详情
    $router->get('info', ['uses' => 'PrizesController@detail']);

    //添加奖品 TODO 校验admin token
    $router->post('add', ['middleware' => 'authAdmin', 'uses' => 'PrizesController@add']);

    //修改奖品 TODO 校验admin token
    $router->post('edit', ['middleware' => 'authAdmin', 'uses' => 'PrizesController@edit']);

    //curl返回奖品列表
    $router->group(['prefix' => 'api', 'middleware' => 'apiAuth'], function () use ($router) {
        $router->get('list', ['uses' => 'PrizesController@apiList']);
    });

    //删除奖品 TODO 校验admin token
    $router->post('del', ['middleware' => 'authAdmin', 'uses' => 'PrizesController@del']);
});

$router->group(['prefix' => 'winner'], function () use ($router) {
    //中奖记录
    $router->get('list', ['uses' => 'WinnerController@list']);

    //抽奖 TODO 校验token
    $router->post('lottery', ['middleware' => 'authToken', 'uses' => 'WinnerController@lottery']);

    //curl返回奖品列表
    $router->group(['prefix' => 'api', 'middleware' => 'apiAuth'], function () use ($router) {
        $router->get('list', ['uses' => 'WinnerController@apiList']);
    });

    //返回用户中奖记录 TODO 校验token
    $router->get('userInfo', ['middleware' => 'authToken', 'uses' => 'WinnerController@userInfo']);

    //返回用户中奖记录 TODO 校验api
    $router->group(['prefix' => 'api', 'middleware' => 'apiAuth'], function () use ($router) {
        $router->post('delUser', ['uses' => 'WinnerController@delUser']);
    });
});

$router->group(['prefix' => 'paricipation'], function () use ($router) {
    //参与人员列表 - 二期修改
    $router->get('list', ['uses' => 'ParicipationController@list']);

    //活动报名 TODO 校验token
    $router->post('join', ['middleware' => 'authToken', 'uses' => 'ParicipationController@add']);

    //退出报名
    //$router->post('out', ['uses' => 'ParicipationController@del']);

    //curl提交报名
    $router->group(['prefix' => 'api', 'middleware' => 'apiAuth'], function () use ($router) {
        $router->get('info', ['uses' => 'ParicipationController@apiInfo']);

        $router->post('join', ['uses' => 'ParicipationController@apiAdd']);

        $router->post('byIds', ['uses' => 'ParicipationController@byIds']);
    });

    //修改奖品 TODO 校验admin token
    $router->post('status', ['middleware' => 'authAdmin', 'uses' => 'ParicipationController@changeStatus']);

    //重置机会
    $router->post('resetChance', ['middleware' => 'authAdmin', 'uses' => 'ParicipationController@resetChance']);
});

$router->group(['prefix' => 'actives'], function () use ($router) {
    //当前正在进行的活动
    $router->get('now', ['uses' => 'ActivesController@nowActive']);

    //活动列表
    $router->get('list', ['uses' => 'ActivesController@list']);

    //添加活动 TODO 校验admin token
    $router->post('add', ['middleware' => 'authAdmin', 'uses' => 'ActivesController@add']);

    //添加活动奖品 TODO 校验admin token
    $router->post('addPrize', ['middleware' => 'authAdmin', 'uses' => 'ActivesController@addActivePrize']);

    //删除活动奖品 TODO 校验admin token
    $router->post('delPrize', ['middleware' => 'authAdmin', 'uses' => 'ActivesController@delActivePrize']);

    //修改活动信息 TODO 校验admin token
    $router->post('edit', ['middleware' => 'authAdmin', 'uses' => 'ActivesController@edit']);

    //删除活动 TODO 校验admin token
    $router->post('del', ['middleware' => 'authAdmin', 'uses' => 'ActivesController@del']);

    //curl返回活动列表
    $router->group(['prefix' => 'api', 'middleware' => 'apiAuth'], function () use ($router) {
        $router->get('list', ['uses' => 'ActivesController@apiList']);
    });

    //删除活动 TODO 校验admin token
    $router->post('editPrize', ['middleware' => 'authAdmin', 'uses' => 'ActivesController@editActivePrize']);
});

//$router->group(['prefix' => 'test'], function () use ($router) {
//    // 测试生成随机奖品和用户
//    $router->get('/', 'TestController@test');
//});

/********************** 二期 *****************************/
$router->group(['prefix' => 'auto'], function () use ($router) {
    $router->get('times', 'AutoController@getActiveTimes');
    //$router->get('lotter', 'AutoController@autoLottery');
    $router->get('record', 'AutoController@rewardList');
    $router->get('status', 'AutoController@status');

//    推送
//    $router->get('test', function(){
//        \GatewayClient\Gateway::$registerAddress = '127.0.0.1:12360';
//        \GatewayClient\Gateway::sendToAll('asdasdasd');
//    });
});
