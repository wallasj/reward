---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://rewards.local.com/docs/collection.json)

<!-- END_INFO -->

#1.活动


Class ActivesController
<!-- START_3b219bdb51afa88d1daf6b1aca50b006 -->
## 1.1 最新活动

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/actives/now" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/now"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {
        "activeTitle": "活动标题",
        "banners": "活动图片",
        "status": "活动状态",
        "persons": "可参与人数",
        "actual_persons": "实际参与人数",
        "description": "奖品描述",
        "beginTime": "开始时间",
        "endTime": "结束时间",
        "timeDays": "抽奖指定日期",
        "prize": {
            "id": "奖品ID",
            "aid": "活动奖品ID",
            "name": "奖品名称",
            "pic": "奖品图片",
            "quantity": "奖品数量"
        }
    }
}
```

### HTTP Request
`GET /actives/now`


<!-- END_3b219bdb51afa88d1daf6b1aca50b006 -->

<!-- START_e1e54aa2c40fe7f0601fa65ab9e94e57 -->
## 1.2 活动列表

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/actives/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {
        "activeTitle": "活动标题",
        "banners": "活动图片",
        "status": "活动状态",
        "persons": "可参与人数",
        "actual_persons": "实际参与人数",
        "description": "奖品描述",
        "beginTime": "开始时间",
        "endTime": "结束时间",
        "timeDays": "抽奖指定日期",
        "prize": {
            "id": "奖品ID",
            "name": "奖品名称",
            "pic": "奖品图片",
            "quantity": "奖品数量"
        }
    }
}
```

### HTTP Request
`GET /actives/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `status` |  optional  | int 活动状态.
    `title` |  optional  | string 活动标题.
    `beginTime` |  optional  | string 开始时间.
    `endTime` |  optional  | string 结束时间.
    `page` |  optional  | int 当前页.
    `pageSize` |  optional  | int 每页显示条数.

<!-- END_e1e54aa2c40fe7f0601fa65ab9e94e57 -->

<!-- START_b412deb356eb4f0568209c0d3e94d045 -->
## 1.3 添加活动

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/actives/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activeTitle":"sed","chance":3,"description":"soluta","persons":6,"beginTime":"quia","endTime":"nihil","timeDays":"odit","banners":"qui"}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activeTitle": "sed",
    "chance": 3,
    "description": "soluta",
    "persons": 6,
    "beginTime": "quia",
    "endTime": "nihil",
    "timeDays": "odit",
    "banners": "qui"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "活动创建成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /actives/add`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activeTitle` | string |  required  | 奖品名称.
        `chance` | integer |  required  | 抽奖次数.
        `description` | string |  optional  | 描述.
        `persons` | integer |  required  | 参与人数.
        `beginTime` | date |  required  | 活动开始时间.
        `endTime` | date |  required  | 活动结束时间.
        `timeDays` | string |  required  | 指定抽奖日期.
        `banners` | string |  optional  | 活动图片.
    
<!-- END_b412deb356eb4f0568209c0d3e94d045 -->

<!-- START_c0492a947ab876c541853c35f2c4c320 -->
## 1.4 添加活动奖品

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/actives/addPrize" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activiteId":"et","prizeId":1,"quantity":11}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/addPrize"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activiteId": "et",
    "prizeId": 1,
    "quantity": 11
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "活动奖品添加成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /actives/addPrize`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activiteId` | string |  required  | 活动ID.
        `prizeId` | integer |  required  | 奖品ID.
        `quantity` | integer |  required  | 奖品数量.
    
<!-- END_c0492a947ab876c541853c35f2c4c320 -->

<!-- START_1b4404da53ae681dfc0f81dbf421a7b7 -->
## 1.5 删除活动奖品

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/actives/delPrize" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activiteId":"asperiores","prizeId":10}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/delPrize"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activiteId": "asperiores",
    "prizeId": 10
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "活动奖品删除成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /actives/delPrize`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activiteId` | string |  required  | 活动ID.
        `prizeId` | integer |  required  | 奖品ID.
    
<!-- END_1b4404da53ae681dfc0f81dbf421a7b7 -->

<!-- START_8d216ae40594bc26662282770f994ab2 -->
## 1.6 修改活动信息

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/actives/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":11,"activeTitle":"excepturi","chance":18,"description":"sed","persons":8,"beginTime":"dignissimos","endTime":"consequatur","timeDays":"similique","banners":"earum"}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": 11,
    "activeTitle": "excepturi",
    "chance": 18,
    "description": "sed",
    "persons": 8,
    "beginTime": "dignissimos",
    "endTime": "consequatur",
    "timeDays": "similique",
    "banners": "earum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "活动修改成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /actives/edit`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | 活动ID.
        `activeTitle` | string |  required  | 奖品名称.
        `chance` | integer |  required  | 抽奖次数.
        `description` | string |  optional  | 描述.
        `persons` | integer |  required  | 参与人数.
        `beginTime` | date |  required  | 活动开始时间.
        `endTime` | date |  required  | 活动结束时间.
        `timeDays` | string |  required  | 指定抽奖日期.
        `banners` | string |  optional  | 活动图片.
    
<!-- END_8d216ae40594bc26662282770f994ab2 -->

<!-- START_53773085db18412eb9cfc846c2a4b179 -->
## 1.7 删除活动

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/actives/del" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":2}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/del"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": 2
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "活动删除成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /actives/del`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | 活动ID.
    
<!-- END_53773085db18412eb9cfc846c2a4b179 -->

<!-- START_c9165de0e424bdeb991526d6f3cae540 -->
## 1.8 最新活动 (API - 内部使用)

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/actives/api/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/api/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`GET /actives/api/list`


<!-- END_c9165de0e424bdeb991526d6f3cae540 -->

<!-- START_005c14c5c01169aa3b2eca7b0745f202 -->
## 1.9 修改活动奖品

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/actives/editPrize" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":"qui","quantity":15}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/actives/editPrize"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": "qui",
    "quantity": 15
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "活动奖品添加成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /actives/editPrize`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | string |  required  | 活动奖品ID.
        `quantity` | integer |  required  | 奖品数量.
    
<!-- END_005c14c5c01169aa3b2eca7b0745f202 -->

#2.参与人员列表


Class ParicipationController
<!-- START_0ffa222abbeabd6bdf05d226c91be230 -->
## 2.1 活动参与者列表

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/paricipation/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/paricipation/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {
        "activeId": "活动ID",
        "activeTitle": "活动标题",
        "userId": "用户ID",
        "userNo": "工号",
        "username": "用户名",
        "isReward": "是否中奖",
        "prizeId": "奖品ID",
        "prize": "奖品名称"
    }
}
```

### HTTP Request
`GET /paricipation/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `activiteId` |  optional  | int 活动ID.
    `reward` |  optional  | int 是否中奖【0未中奖 1中奖】.
    `page` |  optional  | int 当前第几页.
    `pageSize` |  optional  | int 每页显示条数.

<!-- END_0ffa222abbeabd6bdf05d226c91be230 -->

<!-- START_4cfd469263c5f8f6b6b3d6560218dc83 -->
## 2.2 报名参与活动 (token)

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/paricipation/join" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activeId":10,"userId":2}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/paricipation/join"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activeId": 10,
    "userId": 2
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /paricipation/join`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activeId` | integer |  required  | 活动ID.
        `userId` | integer |  required  | 用户ID.
    
<!-- END_4cfd469263c5f8f6b6b3d6560218dc83 -->

<!-- START_f36e340fbb36b08adf69ae174ddbe058 -->
## 2.3 报名信息 (API - 内部使用)

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/paricipation/api/info" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userId":6,"activeId":18}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/paricipation/api/info"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userId": 6,
    "activeId": 18
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`GET /paricipation/api/info`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userId` | integer |  required  | 用户ID.
        `activeId` | integer |  required  | 活动ID.
    
<!-- END_f36e340fbb36b08adf69ae174ddbe058 -->

<!-- START_df59c48a9ae73cd6f656512d8d15078f -->
## 2.4 报名参与活动 (API - 内部使用)

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/paricipation/api/join" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activeId":19,"userId":6}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/paricipation/api/join"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activeId": 19,
    "userId": 6
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /paricipation/api/join`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activeId` | integer |  required  | 活动ID.
        `userId` | integer |  required  | 用户ID.
    
<!-- END_df59c48a9ae73cd6f656512d8d15078f -->

<!-- START_ec80a45d71fa06552c3013f7f558d712 -->
## 2.6 查询用户状态 (API - 后台)

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/paricipation/api/byIds" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"ids":"alias"}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/paricipation/api/byIds"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "ids": "alias"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /paricipation/api/byIds`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `ids` | string |  required  | 用户ID.
    
<!-- END_ec80a45d71fa06552c3013f7f558d712 -->

<!-- START_57b2a807228ae00eb2be8e5d64d8b7db -->
## 2.5 报名参与活动 (API - 后台)

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/paricipation/status" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activeId":16,"userId":13,"status":18}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/paricipation/status"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activeId": 16,
    "userId": 13,
    "status": 18
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /paricipation/status`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activeId` | integer |  required  | 活动ID.
        `userId` | integer |  required  | 用户ID.
        `status` | integer |  required  | 状态 0无效 1有效.
    
<!-- END_57b2a807228ae00eb2be8e5d64d8b7db -->

<!-- START_595b4fe1e3affc8d698442d26ec7add2 -->
## 2.7 重置抽奖机会 (API - 后台)

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/paricipation/resetChance" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/paricipation/resetChance"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /paricipation/resetChance`


<!-- END_595b4fe1e3affc8d698442d26ec7add2 -->

#3.奖品


Class PrizesController
<!-- START_4fa13de2c5ef7d3b433c68bf4e61404a -->
## 3.1 奖品列表

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/prizes/index" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/prizes/index"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {
        "id": "奖品ID",
        "name": "奖品名称",
        "imgs": "奖品图片",
        "description": "奖品描述",
        "level": "奖品等级 【1 一等奖 2二等奖 3三等奖】",
        "price": "奖品价值",
        "stock": "奖品库存",
        "status": "奖品状态【1上架 0下架】"
    }
}
```

### HTTP Request
`GET /prizes/index`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `status` |  optional  | int 奖品状态【1上架 0下架】.
    `stock` |  optional  | int 库存.
    `page` |  optional  | int 当前页.
    `pageSize` |  optional  | int 每页显示条数.

<!-- END_4fa13de2c5ef7d3b433c68bf4e61404a -->

<!-- START_f9233afc06d93b02b23403c1532ffcd6 -->
## 3.2 奖品详情

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/prizes/info" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/prizes/info"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {
        "id": "奖品ID",
        "name": "奖品名称",
        "imgs": "奖品图片",
        "description": "奖品描述",
        "level": "奖品等级 【1 一等奖 2二等奖 3三等奖】",
        "price": "奖品价值",
        "stock": "奖品库存",
        "status": "奖品状态【1上架 0下架】"
    }
}
```

### HTTP Request
`GET /prizes/info`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int 奖品ID.

<!-- END_f9233afc06d93b02b23403c1532ffcd6 -->

<!-- START_3ab80d03490aa95f839c7d42da746fd1 -->
## 3.3 添加奖品

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/prizes/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"autem","level":14,"status":6,"imgs":"enim","description":"eaque","price":44,"stock":16}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/prizes/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "autem",
    "level": 14,
    "status": 6,
    "imgs": "enim",
    "description": "eaque",
    "price": 44,
    "stock": 16
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "奖品添加成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /prizes/add`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 奖品名称.
        `level` | integer |  required  | 奖品等级 【1 一等奖 2二等奖 3三等奖】.
        `status` | integer |  required  | 奖品是否上架 【1上架 0下架】.
        `imgs` | string |  optional  | 奖品图片.
        `description` | string |  optional  | 奖品描述.
        `price` | float |  optional  | 奖品金额.
        `stock` | integer |  optional  | 奖品库存.
    
<!-- END_3ab80d03490aa95f839c7d42da746fd1 -->

<!-- START_51283aca1160c38249a23d6f7786226a -->
## 3.4 修改奖品

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/prizes/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":13,"name":"harum","level":4,"status":4,"imgs":"vitae","description":"fuga","price":1.39,"stock":4}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/prizes/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": 13,
    "name": "harum",
    "level": 4,
    "status": 4,
    "imgs": "vitae",
    "description": "fuga",
    "price": 1.39,
    "stock": 4
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "奖品信息修改成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /prizes/edit`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | 奖品ID.
        `name` | string |  required  | 奖品名称.
        `level` | integer |  required  | 奖品等级 【1 一等奖 2二等奖 3三等奖】.
        `status` | integer |  required  | 奖品是否上架 【1上架 0下架】.
        `imgs` | string |  optional  | 奖品图片.
        `description` | string |  optional  | 奖品描述.
        `price` | float |  optional  | 奖品金额.
        `stock` | integer |  optional  | 奖品库存.
    
<!-- END_51283aca1160c38249a23d6f7786226a -->

<!-- START_f6c024c2d0f4d36b58a80f6bcf3f8145 -->
## 3.5 奖品列表 (API - 内部使用)

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/prizes/api/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/prizes/api/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "操作成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`GET /prizes/api/list`


<!-- END_f6c024c2d0f4d36b58a80f6bcf3f8145 -->

<!-- START_5d3687c70f7eff010400e77ae64d61b2 -->
## 3.5 删除奖品

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/prizes/del" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":3}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/prizes/del"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": 3
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "奖品信息删除成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /prizes/del`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `id` | integer |  required  | 奖品ID.
    
<!-- END_5d3687c70f7eff010400e77ae64d61b2 -->

#4.中奖


Class WinnerController
<!-- START_ad124e93c7f8813c4ca0f3b444bdc000 -->
## 4.1 中奖记录列表

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/winner/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/winner/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {
        "activeTitle": "活动标题",
        "prizeName": "奖品名称",
        "userId": "用户ID",
        "username": "用户名"
    }
}
```

### HTTP Request
`GET /winner/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `activiteId` |  optional  | int 活动ID.
    `userId` |  optional  | int 用户ID.
    `deptId` |  optional  | int 部门ID.
    `page` |  optional  | int 当前第几页.
    `pageSize` |  optional  | int 每页显示条数.

<!-- END_ad124e93c7f8813c4ca0f3b444bdc000 -->

<!-- START_3fbb61686879e6930643749776a2577c -->
## 4.2 抽奖 (token)

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/winner/lottery" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activiteId":18,"userId":6}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/winner/lottery"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activiteId": 18,
    "userId": 6
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "恭喜中奖 \/ 重在参与",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /winner/lottery`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activiteId` | integer |  required  | 活动ID.
        `userId` | integer |  required  | 用户ID.
    
<!-- END_3fbb61686879e6930643749776a2577c -->

<!-- START_04f926dcf846814e09004bbf2c79db31 -->
## 4.3 中奖记录列表 (API - 内部使用)

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/winner/api/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://rewards.local.com/winner/api/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`GET /winner/api/list`


<!-- END_04f926dcf846814e09004bbf2c79db31 -->

<!-- START_0d59e1608a6ce996c7eb842d309085c3 -->
## 4.4 当前用户中奖记录

> Example request:

```bash
curl -X GET \
    -G "http://rewards.local.com/winner/userInfo" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"activeId":12}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/winner/userInfo"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "activeId": 12
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "处理成功",
    "code": 0,
    "data": {
        "prizeId": "奖品ID",
        "name": "奖品名称",
        "level": "奖品等级"
    }
}
```

### HTTP Request
`GET /winner/userInfo`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `activeId` | integer |  required  | 活动ID.
    
<!-- END_0d59e1608a6ce996c7eb842d309085c3 -->

<!-- START_57a0fbed6029eab5904a4be5280286bb -->
## 4.5 获取用户当前状态

> Example request:

```bash
curl -X POST \
    "http://rewards.local.com/winner/api/delUser" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userId":12}'

```

```javascript
const url = new URL(
    "http://rewards.local.com/winner/api/delUser"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userId": 12
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "可以清除",
    "code": 0,
    "data": {}
}
```

### HTTP Request
`POST /winner/api/delUser`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userId` | integer |  required  | 用户ID.
    
<!-- END_57a0fbed6029eab5904a4be5280286bb -->


