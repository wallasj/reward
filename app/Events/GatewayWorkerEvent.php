<?php


namespace App\Events;

use App\Models\Actives;
use App\Models\ActivesPrizes;
use GatewayWorker\Lib\Gateway;
use Illuminate\Support\Facades\Log;

class GatewayWorkerEvent
{
    public static function onWorkerStart($businessWorker)
    {
        echo "BusinessWorker    Start\n";
    }

    public static function onConnect($client_id)
    {
        Gateway::sendToClient($client_id, json_encode(['type' => 'init', 'client_id' => $client_id]));
    }

    public static function onWebSocketConnect($client_id, $data)
    {
        Log::info('onWebSocketConnect' . $client_id . ' data ' . json_encode($data));
    }

    public static function onMessage($client_id, $message)
    {
        $response = ['code' => 0, 'msg' => 'ok', 'data' => []];
        $message = json_decode($message);

        if (!isset($message->mode)) {
            $response['msg'] = 'missing parameter mode';
            $response['code'] = 3306;
            Gateway::sendToClient($client_id, json_encode($response));
            return false;
        }

//        switch ($message->mode) {
//            default:
//                $response['code'] = 404;
//                $response['msg'] = 'Undefined';
//            break;
//        }

        Gateway::sendToClient($client_id, json_encode($response));
    }

    public static function onClose($client_id)
    {
        Log::info('close connection' . $client_id);
    }

}
