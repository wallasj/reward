<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Ixudra\Curl\Facades\Curl;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token') || $request->header('Token')) {
                $token = $request->header('Token') ? $request->header('Token') : $request->input('api_token');
                $result = Curl::to(config('config.user_api') . 'api/check')->withData([
                    'token' => $token,
                    'apiSecret' => config('config.user_secret')
                ])->post();
                $response = json_decode($result);
                if ($response && isset($response->data)) {
                    setRcache($token, json_encode($response->data));
                    return true;
                }
            }
            return false;
        });
    }
}
