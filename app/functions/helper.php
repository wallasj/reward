<?php
/**
 *
 * User: wallas
 * Date: 2019/11/28
 * Description: 全局函数
 */

declare(strict_types=1);

/**
 * 小数点过滤
 * $amount 需要过滤的金额
 * $len 小数点保留长度
 * $withFormat 是否显示 1,000 科学计数格式
 */
if (!function_exists('amountFilter')) {
    function amountFilter($amount, int $len = 2, bool $withFormat = false) {
        // 非法数字,就不要返回了
        if (!is_numeric($amount)) {
            return intval($amount);
        }

        //如果为 0 直接返回
        if ($amount == 0) {
            return '0.00';
        }

        //返回整数
        if ($len === 0) {
            return number_format(intval($amount));
        }

        //如果是整数
        if (is_int($amount)) {
            if ($withFormat) {
                $amount = number_format($amount) . str_pad('.', $len, '0');
            } else {
                $amount = $amount . str_pad('.', $len, '0');
            }
        } else {
            //小数点分割
            $tem_arr = explode('.', (string)$amount);
            if (strlen($tem_arr[1]) <= $len) {
                if ($withFormat) {
                    $amount = number_format($tem_arr[0]) . '.' . str_pad($tem_arr[1], $len, '0');
                } else {
                    $amount = $tem_arr[0] . '.' . str_pad($tem_arr[1], $len, '0');
                }
            } else if (strlen($tem_arr[1]) > $len) {
                if ($withFormat) {
                    $amount = number_format($tem_arr[0]) . '.' . substr($tem_arr[1], 0, $len);
                } else {
                    $amount = $tem_arr[0] . '.' . substr($tem_arr[1], 0, $len);
                }
            }
        }
        return $amount;
    }
}

if (!function_exists('isHttps')) {
    function isHttps()
    {
        if ( ! empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
            return true;
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
            return true;
        } else if ( ! empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
            return true;
        }
        return false;
    }
}

//redis
if (!function_exists('setRcache')) {
    function setRcache(string $name, $value, int $expires = 3600)
    {
        if (!empty($name)) {
            \Illuminate\Support\Facades\Redis::setex($name, $expires, $value);
        }
    }
}

if (!function_exists('getRcache')) {
    function getRcache(string $name)
    {
        if (!empty($name)) {
            if (\Illuminate\Support\Facades\Redis::exists($name))
            {
                return \Illuminate\Support\Facades\Redis::get($name);
            }
        }
        return '';
    }
}

//log
if (!function_exists('logRecord')) {
    function logRecord($info, string $type = 'info')
    {
        $route = \Illuminate\Support\Facades\Route::current()->getActionName();
        if ($type === 'info') {
            \Illuminate\Support\Facades\Log::info($route . ' ' . $info);
            return ;
        } else if($type === 'warning') {
            \Illuminate\Support\Facades\Log::warning($route . ' ' . $info);
            return ;
        }
        \Illuminate\Support\Facades\Log::error($route . ' ' . $info);
    }
}

/**
 * 分页计算
 * $count 总数
 * $nowPage 当前页
 * $limit 每页多少条
 */
if (!function_exists('getTotalPage'))
{
    function getTotalPage($count, int $nowPage, int $limit)
    {
        //默认15条
        if ($limit <= 0) $limit = config('app.limit');

        if ($nowPage == 0) $nowPage = config('app.pageSize');

        $nowPage = ($nowPage - 1) * $limit;

        $p =  (int)($count / $limit);
        $totalPage = $count % $limit == 0 ? $p : $p + 1;
        return [
            'total' => $totalPage,
            'page' => $nowPage,
            'limit' => $limit
        ];
    }
}

//当前时间
if (!function_exists('nowDate'))
{
    function nowDate(string $timeZone = 'Asia/Shanghai')
    {
        if ($timeZone === 'US') {
            $timeZone = 'US/Eastern';
        }
        return \Carbon\Carbon::now($timeZone)->format('Y-m-d H:i:s');
    }
}

if (!function_exists('timeRange'))
{
    function timeRange(string $type = 'today', string $timeZone = 'Asia/Shanghai')
    {
        $time = '';

        if ($timeZone === 'US') {
            $timeZone = 'US/Eastern';
        }

        $current = \Carbon\Carbon::now($timeZone);

        switch ($type) {
            case 'today':
                $time = $current->today();
            break;
            case 'yesterday':
                $time = $current->yesterday();
                break;
            case 'tomorrow':
                $time = $current->tomorrow();
                break;
            case 'weekStart':
                $time = $current->startOfWeek();
            break;
            case 'weekEnd':
                $time = $current->endOfWeek();
            break;
            case 'monthStart':
                $time = $current->startOfMonth();
            break;
            case 'lastMonthStart':
                $time = $current->subMonth()->firstOfMonth();
            break;
            case 'lastMonthEnd':
                $time = $current->subMonth()->endOfMonth();
            break;
            case 'yearStart':
                $time = $current->startOfYear();
            break;
        }

        return !empty($time) ? $time->format('Y-m-d H:i:s') : '';
    }
}

if (!function_exists('isDate'))
{
    function isDate( $dateString ) {
        if (!strtotime($dateString)) return false;
        return strtotime( date('Y-m-d', strtotime($dateString)) ) === strtotime( $dateString );
        /*date函数会给月和日补零，所以最终用unix时间戳来校验*/
    }
}

if (!function_exists('nextTime'))
{
    function nextTime($timeDays, $timeHour, $timeMinutes) {
        if (empty($timeDays)) {
            return false;
        }

        $hours = ' ' . str_pad((string)$timeHour, 2, '0', STR_PAD_LEFT) . ':' . str_pad((string)$timeMinutes, 2, '0', STR_PAD_LEFT) . ':00';

        //$timeDays = '4;22';

        $now = time();
        $dayCheck = false;
        if (strpos($timeDays, ';') === false) {
            $_day = str_pad($timeDays, 2, '0', STR_PAD_LEFT);
            $beginTime = date('Y-m-'.$_day . $hours);
            $endTime = date('Y-m-'.$_day . ' 23:59:59');
            if ($now >= strtotime($beginTime) && $now <= strtotime($endTime)) {
                $dayCheck = true;
            }
        } else {
            $timeDays = explode(';', $timeDays);
            foreach ($timeDays as $val) {
                $_day = str_pad($val, 2, '0', STR_PAD_LEFT);
                $beginTime = date('Y-m-'.$_day . $hours);
                $endTime = date('Y-m-'.$_day . ' 23:59:59');
                if ($now >= strtotime($beginTime) && $now <= strtotime($endTime)) {
                    $dayCheck = true;
                    break;
                }
            }
        }

        if (!$dayCheck) {
            return false;
        }

        return true;
    }
}
