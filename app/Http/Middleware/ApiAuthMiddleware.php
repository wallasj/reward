<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('apiSecret')) {
            $apiSecret = $request->input('apiSecret', '');
            if ($apiSecret == 'tg4b_jxzx_reward') {
                return $next($request);
            }
        }
        return response()->json([
            'message' => '权限不足',
            'code' => 401,
            'data' => '',
        ]);
    }
}
