<?php

namespace App\Http\Middleware;

use Closure;
use Ixudra\Curl\Facades\Curl;

class AuthAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('api_token') || $request->header('Token')) {
            $token = $request->header('Token') ? $request->header('Token') : $request->input('api_token');
            $result = Curl::to(config('config.user_api') . 'api/checkAdmin')->withData([
                'token' => $token,
                'apiSecret' => config('config.user_secret')
            ])->post();
            $response = json_decode($result);
            if ($response && isset($response->data)) {
                setRcache($token, json_encode($response->data));
                return $next($request);
            }
        }
        return response()->json([
            'message' => '权限不足',
            'code' => 401,
            'data' => '',
        ]);
    }
}
