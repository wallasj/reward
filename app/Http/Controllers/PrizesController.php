<?php

namespace App\Http\Controllers;

use App\Models\ActivesPrizes;
use App\Models\Prizes;
use App\Models\WinnerRecord;
use Illuminate\Http\Request;

/**
 * @group 3.奖品
 *
 * Class PrizesController
 *
 * @package App\Http\Controllers
 */
class PrizesController extends Controller
{

    /**
     * 3.1 奖品列表
     *
     * @urlParam status int 奖品状态【1上架 0下架】.
     * @urlParam stock int 库存.
     * @urlParam page int 当前页.
     * @urlParam pageSize int 每页显示条数.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *          "id": "奖品ID",
     *          "name": "奖品名称",
     *          "imgs": "奖品图片",
     *          "description": "奖品描述",
     *          "level": "奖品等级 【1 一等奖 2二等奖 3三等奖】",
     *          "price": "奖品价值",
     *          "stock": "奖品库存",
     *          "status": "奖品状态【1上架 0下架】"
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $status = $request->get('status', '');

        //分页参数
        $nowPage = $request->get('page', config('config.page'));

        $pageSize = $request->get('pageSize', config('config.pageSize'));

        $query = Prizes::query();

        if (is_numeric($status)) {
            $query->where('status', intval($status));
        }

        $name = $request->get('name', '');
        if (!empty($name)) {
            $query->where('name', 'like', "%{$name}%");
        }

        $stock = $request->get('stock', 0);
        if (intval($stock) > 0) {
            $query->where('stock', '>', 0);
        }

        $count = $query->count();

        if ($count > 0) {
            //TODO 分页
            $pagenation = getTotalPage($count, (int)$nowPage, (int)$pageSize);
            $list = $query
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get();

            return $this->successResponse([
                'list' => $list,
                'count' => $count,
                'totalPage' => $pagenation['total']
            ]);
        }

        return $this->successNullResponse();
    }

    /**
     * 3.2 奖品详情
     *
     * @urlParam id int 奖品ID.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *          "id": "奖品ID",
     *          "name": "奖品名称",
     *          "imgs": "奖品图片",
     *          "description": "奖品描述",
     *          "level": "奖品等级 【1 一等奖 2二等奖 3三等奖】",
     *          "price": "奖品价值",
     *          "stock": "奖品库存",
     *          "status": "奖品状态【1上架 0下架】"
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail(Request $request)
    {
        $id = $request->get('id', '');

        if (intval($id) > 0) {
            $data = Prizes::where('id', $id)->first();
            if ($data) {
                return $this->successResponse($data);
            }
            return $this->successNullResponse();
        }

        return $this->errorResponse('缺少参数');
    }

    /**
     * 3.3 添加奖品
     *
     * @bodyParam name string required 奖品名称.
     * @bodyParam level int required 奖品等级 【1 一等奖 2二等奖 3三等奖】.
     * @bodyParam status int required 奖品是否上架 【1上架 0下架】.
     * @bodyParam imgs string 奖品图片.
     * @bodyParam description string 奖品描述.
     * @bodyParam price float 奖品金额.
     * @bodyParam stock int 奖品库存.
     *
     * @response {
     *       "message": "奖品添加成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add(Request $request)
    {
        $this->validate($request, Prizes::$_validRule, Prizes::$_messages);
        $data = $request->all();

        $prizes = Prizes::create($data);
        if ($prizes) {
            return $this->successResponse(['id' => $prizes->id], '奖品添加成功');
        }

        return $this->errorResponse('奖品添加失败');
    }

    /**
     * 3.4 修改奖品
     *
     * @bodyParam id int required 奖品ID.
     * @bodyParam name string required 奖品名称.
     * @bodyParam level int required 奖品等级 【1 一等奖 2二等奖 3三等奖】.
     * @bodyParam status int required 奖品是否上架 【1上架 0下架】.
     * @bodyParam imgs string 奖品图片.
     * @bodyParam description string 奖品描述.
     * @bodyParam price float 奖品金额.
     * @bodyParam stock int 奖品库存.
     *
     * @response {
     *       "message": "奖品信息修改成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit(Request $request)
    {
        $id = $request->get('id', '');

        if (intval($id) > 0) {
            $prize = Prizes::where('id', $id)->first();

            if ($prize) {
                $data = $request->all();
                Prizes::$_validRule['name'] = Prizes::$_validRule['name'] . ',name,' . $id;
                $this->validate($request, Prizes::$_validRule, Prizes::$_messages);
                unset($data['id']);
                $prizes = $prize->where('id', $id)->update($data);
                if ($prizes) {
                    return $this->successResponse([], '奖品信息修改成功');
                }
                return $this->errorResponse('奖品信息修改失败');
            }
            return $this->errorResponse('未找到奖品记录');
        }

        return $this->errorResponse('缺少参数');
    }

    /**** for api ****/

    /**
     * 3.5 奖品列表 (API - 内部使用)
     *
     * @response {
     *       "message": "操作成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function apiList()
    {
        $query = Prizes::query();

        $query->where('stock', '>', 0);

        $query->where('status', 1);

        $count = $query->count();

        if ($count > 0) {
            $datas = [];
            $list = $query->select('name', 'level')
                ->orderBy('level', 'asc')
                ->get();

            foreach ($list as $key => $val) {
                if (isset(Prizes::level[$val->level])) {
                    $datas[$key] = [
                        'name' => (Prizes::level[$val->level]) . ' : ' . $val->name
                    ];
                }
            }

            return $this->successResponse($datas);
        }

        return $this->successNullResponse();
    }

    /**
     * 3.5 删除奖品
     *
     * @bodyParam id int required 奖品ID.
     *
     * @response {
     *       "message": "奖品信息删除成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function del(Request $request)
    {
        $id = $request->get('id', '');

        if (intval($id) > 0) {
            if ($id == 4) {
                return $this->errorResponse('该奖品不允许被删除');
            }

            $prize = Prizes::where('id', $id)->first();

            if ($prize) {
                //TODO 判断中奖记录或者奖品列表中是否有相应的奖品
                $winnerCount = WinnerRecord::where('prizeId', $id)->count();
                $activePrizeCount = ActivesPrizes::where('prizeId', $id)->count();
                if ($winnerCount > 0 || $activePrizeCount > 0) {
                    return $this->errorResponse( '该奖品已存在于抽奖结果或奖品列表中，无法删除');
                }

                $delete = $prize->delete();
                if ($delete) {
                    return $this->successResponse([], '奖品删除成功');
                }
                return $this->errorResponse('奖品删除失败');
            }

            return $this->errorResponse('未找到奖品记录');
        }

        return $this->errorResponse('缺少参数');
    }

}
