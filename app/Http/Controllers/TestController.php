<?php


namespace App\Http\Controllers;


use App\Services\FixedCalculationServices;

class TestController extends Controller {

    public function test() {
        $service = new FixedCalculationServices();
        return $service->reward();
    }
}
