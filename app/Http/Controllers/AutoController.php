<?php


namespace App\Http\Controllers;

use App\Models\Actives;
use App\Models\ActivesPrizes;
use App\Models\WinnerRecord;
use App\Services\FixedCalculationServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

class AutoController extends Controller {

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    //获取活动时间
    public function getActiveTimes() {
        $query = Actives::query();
        $query->select('timeDays', 'timeHour', 'timeMinutes');
        $query->where('status', 1);
        $active = $query->first();
        if ($active) {
            //判断活动是否是指定日期
            $timeDays = $active->timeDays;
            if (empty($timeDays)) {
                return $this->errorResponse('活动未开始');
            }

            //获取今日是几号
            $day = intval(date('d'));
            //$timeDays = 27;
            //$day = 28;

            do {
                $hours = ' ' . str_pad($active->timeHour, 2, '0', STR_PAD_LEFT) . ':' . str_pad($active->timeMinutes, 2, '0', STR_PAD_LEFT) . ':00';
                //如果是单日
                if (strpos($timeDays, ';') === false) {
                    $st = strtotime(date('Y-m-'.str_pad($timeDays, 2, '0', STR_PAD_LEFT)) . $hours);
                    if (time() <= $st) {
                        $activeTime = [
                            'nextTime' => date('Y-m-'.str_pad($timeDays, 2, '0', STR_PAD_LEFT)) . $hours
                        ];
                    } else {
                        $activeTime = [
                            'nextTime' => date('Y-m-'.str_pad($timeDays, 2, '0', STR_PAD_LEFT), strtotime('+1 month')) . $hours
                        ];
                    }
                    break;
                } else {
                    $timeDays = explode(';', $timeDays);
                    //判断日期是否已经在日期中
                    if (in_array($day, $timeDays)) {
                        $st = strtotime(date('Y-m-'.str_pad($day, 2, '0', STR_PAD_LEFT)) . $hours);
                        //判断当前时间是否小于开始时间
                        if (time() <= $st) {
                            $activeTime = [
                                'nextTime' => date('Y-m-'.str_pad($day, 2, '0', STR_PAD_LEFT)) . $hours
                            ];
                        } else if (end($timeDays) == $day){
                            $activeTime = [
                                'nextTime' => date('Y-m-'.str_pad(current($timeDays), 2, '0', STR_PAD_LEFT), strtotime('+1 month')) . $hours
                            ];
                        } else {
                            $key = array_search($day, $timeDays);
                            $activeTime = [
                                'nextTime' => date('Y-m-'.str_pad($timeDays[$key + 1], 2, '0', STR_PAD_LEFT)) . $hours
                            ];
                        }
                        break;
                    }

                    $timeDays[] = intval($day);
                    sort($timeDays);
                    //判断当日是不是最后一天
                    if (end($timeDays) == $day) {
                        //如果是最后一天，那就取下个月第一天
                        $activeTime = [
                            'nextTime' => date('Y-m-'.str_pad(current($timeDays), 2, '0', STR_PAD_LEFT), strtotime('+1 month')) . $hours
                        ];
                    } else {
                        $key = array_search($day, $timeDays);
                        $activeTime = [
                            'nextTime' => date('Y-m-'.str_pad($timeDays[$key + 1], 2, '0', STR_PAD_LEFT)) . $hours
                        ];
                    }
                }
            } while(0);
            //返回活动时间
            return $this->successResponse($activeTime);
        }
        return $this->errorResponse('没有活动');
    }

    //抽奖
    public function autoLottery()
    {
        //判断时间是否满足条件
        $query = Actives::query();
        $query->select('id', 'timeDays', 'timeHour', 'timeMinutes');
        $query->where('status', 1);
        $active = $query->first();
        if ($active) {
            //检查活动是否开始
            if (nextTime($active->timeDays, $active->timeHour, $active->timeMinutes) || 1) {
                //活动已开始
                $quantity = ActivesPrizes::where('activiteId', $active->id)
                    ->sum('quantity');
                if ($quantity <= 0) {
                    try {
                        $res = json_encode([
                            'code' => 404,
                            'msg' => '当前活动奖品不足'
                        ]);
                    } catch (\Exception $e) {
                        Log::info('autoLottery-活动奖品不足， 异常:' . $e->getMessage());
                    }
                    Log::info('autoLottery-活动奖品不足');
                }

                //获取奖品列表
                $prize = ActivesPrizes::where('activiteId', $active->id)
                    ->where('quantity', '>', 0)
                    ->get();
                $prizes = [];
                foreach ($prize as $val) {
                    $_p = $val->prizeInfo;
                    $prizes[] = [
                        'pid' => $_p->id,
                        'pname' => $_p->name,
                        'plevel' => $_p->level,
                        'pnum' => $val->quantity
                    ];
                }

                //判断用户是否已经中奖
                $begin = date('Y-m-d 00:00:00');
                $end = date('Y-m-d 23:59:59');
                $winnerRecord = WinnerRecord::whereBetween('created_at', [$begin, $end])
                    ->select('userId')
                    ->get();
                $notIn = [];
                if (!$winnerRecord->isEmpty()) {
                    foreach ($winnerRecord as $wr) {
                        $notIn[] = $wr->userId;
                    }
                }

                //获取所有用户
                $users = [];
                $result = Curl::to(config('config.user_api') . '/api/allUsers')->withData([
                    'notIn' => $notIn,
                    'apiSecret' => config('config.user_secret')
                ])->post();
                $response = json_decode($result);
                Log::info('autoLottery-用户信息 : ' . json_encode($result));
                if ($response && $response->data) {
                    foreach ($response->data as $val) {
                        $users[] = [
                            'uid' => $val->id,
                            'uname' => $val->username,
                            'userno' => $val->userno
                        ];
                    }
                }

                if (empty($users)) {
                    try {
                        $res = json_encode([
                            'code' => 404,
                            'msg' => '缺少参与用户'
                        ]);
                    } catch (\Exception $e) {
                        Log::info('autoLottery-缺少参与用户， 异常:' . $e->getMessage());
                    }
                    Log::info('autoLottery-缺少参与用户');
                }

                if ($prizes && $users) {
                    //执行抽奖
                    $service = new FixedCalculationServices();
                    $result = $service->reward($prizes, $users);

                    try {
                        DB::beginTransaction();
                        //TODO 增加中奖记录
                        $winnerRecord = WinnerRecord::create([
                            'activiteId' => $active->id,
                            'userId' => $result['uid'],
                            'prizeId' => $result['prize_id'],
                            'name' => $result['uname'],
                            'level' => $result['plevel'],
                            'status' => 1
                        ]);

                        Log::info('autoLottery-更新信息 winnerRecord ' . json_encode($winnerRecord));
                        if (!$winnerRecord) {
                            Log::info('autoLottery-更新信息回滚 winnerRecord ' . json_encode($winnerRecord));
                            DB::rollBack();
                        }

                        //减少奖品数量
                        $quantity = ActivesPrizes::where([
                            'activiteId' => $active->id,
                            'prizeId' => $result['prize_id']
                        ])->decrement('quantity');
                        Log::info('autoLottery-更新信息 quantity ' . json_encode($quantity));
                        if (!$quantity) {
                            Log::info('autoLottery-更新信息回滚 quantity');
                            DB::rollBack();
                        }

                        DB::commit();
                        try {
                            $res = json_encode([
                                'code' => 200,
                                'msg' => '恭喜中奖'
                            ]);
                        } catch (\Exception $e) {
                            Log::info('autoLottery-恭喜中奖， 异常:' . $e->getMessage());
                        }
                    } catch(\Exception $e) {
                        Log::info('autoLottery-事务异常 update ' . $e->getMessage());
                    }
                }
                Log::info('autoLottery-抽奖失败');
            }
        }
    }

    //返回中奖列表
    public function rewardList()
    {
        $query = WinnerRecord::query();
        $begin = date('Y-m-d 00:00:00');
        $end = date('Y-m-d 23:59:59');
        $query->whereBetween('created_at', [$begin, $end]);
        $count = $query->count();

        $datas = [];
        if ($count > 0) {
            $list = $query->get();
            $userIds = $check = [];
            foreach ($list as $key => $val) {
                $userIds[] = $val['userId'];
                $datas[$key] = [
                    'activeTitle' => $val->activeInfo->activeTitle,
                    'prizeName' => $val->name,
                    'userId' => $val->userId,
                    'username' => '无名氏',
                    'created_at' => date('Y-m-d', strtotime($val->created_at))
                ];
            }

            if (!empty($userIds)) {
                $result = Curl::to(config('config.user_api') . '/api/byIds')->withData([
                    'ids' => $userIds,
                    'apiSecret' => config('config.user_secret')
                ])->post();
                $response = json_decode($result);
                Log::info('rewardList-用户信息 : ' . json_encode($result));
                if ($response && $response->data) {
                    foreach ($response->data as $val) {
                        $check[$val->id] = [
                            'uname' => $val->username,
                            'userno' => $val->userno
                        ];
                    }
                }
            }

            if ($check) {
                foreach ($datas as $key => $val) {
                    if (isset($check[$val['userId']])) {
                        $datas[$key]['userno'] = $check[$val['userId']]['userno'];
                        $datas[$key]['username'] = $check[$val['userId']]['uname'];
                    }
                }
            }
        }

        return $this->successResponse($datas);
    }

    //获取抽奖状态
    public function status()
    {
        $query = Actives::query();
        $query->select('id', 'timeDays', 'timeHour', 'timeMinutes');
        $query->where('status', 1);
        $active = $query->first();
        if ($active) {
            //检查活动是否开始
            if (nextTime($active->timeDays, $active->timeHour, $active->timeMinutes)) {
                //活动已开始
                $quantity = ActivesPrizes::where('activiteId', $active->id)
                    ->sum('quantity');
                if ($quantity <= 0) {
                    //奖品已抽完
                    return $this->successResponse([], '奖品已抽完');
                }
                //抽奖中
                return $this->successResponse([], '活动进行中');
            }
            return $this->errorResponse('活动未开始');
        }
        return $this->errorResponse('没有活动');
    }

}
