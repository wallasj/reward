<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public $userInfo;

    public function __construct(Request $request)
    {
        $token = $request->header('Token') ? $request->header('Token') : $request->input('api_token');
        if ($token) {
            $cache = getRcache((string)$token);
            if ($cache) {
                $this->userInfo = json_decode($cache);
            }
        }
    }

    //
    public function errorResponse(string $message = "处理失败", array $data = [], int $code = -1)
    {
        return response()->json([
            'message' => $message,
            'code' => $code,
            'data' => $data,
        ]);
    }

    public function successResponse($data = [], string $message = "处理成功", int $code = 0)
    {
        if (is_null($data)) $data = [];
        return response()->json([
            'message' => $message,
            'code' => $code,
            'data' => $data,
        ]);
    }

    public function successNullResponse(string $message = "处理成功", int $code = 0)
    {
        return response()->json([
            'message' => $message,
            'code' => $code,
            'data' => [
                'list' => [],
                'count' => 0,
                'totalPage' => 0
            ],
        ]);
    }

    //重置
    private function _reset($key = '')
    {
        Redis::set($key, null);
    }
}
