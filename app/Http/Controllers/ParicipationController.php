<?php

namespace App\Http\Controllers;

use App\Models\Actives;
use App\Models\ParicipationList;
use App\Models\WinnerRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

/**
 * @group 2.参与人员列表
 *
 * Class ParicipationController
 *
 * @package App\Http\Controllers
 */
class ParicipationController extends Controller
{

    /**
     * 2.1 活动参与者列表
     *
     * @urlParam activiteId int 活动ID.
     * @urlParam reward int 是否中奖【0未中奖 1中奖】.
     * @urlParam date date 查询时间.
     * @urlParam page int 当前第几页.
     * @urlParam pageSize int 每页显示条数.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *          "activeId": "活动ID",
     *          "activeTitle": "活动标题",
     *          "userId": "用户ID",
     *          "userNo": "工号",
     *          "username": "用户名",
     *          "isReward": "是否中奖",
     *          "prizeId": "奖品ID",
     *          "prize": "奖品名称"
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $query = ParicipationList::query();

        $activeId = $request->input('activeId');

        if (is_numeric($activeId)) {
            $query->where('activeId', $activeId);
        }

        $isReward = $request->input('reward', '');

        if (is_numeric($isReward)) {
            $query->where('isReward', intval($isReward));
        }

        $date = $request->input('date', '');
        if ($date) {
            if (strtotime($date) !== false) {
                $begin = date('Y-m-d 00:00:00', strtotime($date));
                $end = date('Y-m-d 23:59:59', strtotime($date));
                $query->whereBetween('created_at', [$begin, $end]);
            }
        }

        //分页参数
        $nowPage = $request->get('page', config('config.page'));

        $pageSize = $request->get('pageSize', config('config.pageSize'));

        $count = $query->count();

        if ($count > 0) {
            $pagenation = getTotalPage($count, (int)$nowPage, (int)$pageSize);
            $list = $query
                ->select('activeId', 'userId', 'isReward', 'prizeId', 'chance')
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get();

            $datas = $list->toArray();
            $userIds = $check = [];
            foreach ($list as $key => $val) {
                $userIds[] = $val['userId'];
                $active = $val->active;
                $datas[$key]['activeTitle'] = $active->activeTitle;
                if ($val->chance == 0) {
                    $datas[$key]['prize'] = '未中奖';
                } else {
                    $datas[$key]['prize'] = '未抽奖';
                }
                if ($val->prizeId > 0) {
                    $prize = $val->prize;
                    $datas[$key]['level'] = $prize->level;
                    $datas[$key]['prize'] = $prize->name;
                }
                $datas[$key]['userNo'] = '';
                $datas[$key]['username'] = '无名氏';
            }

            if (!empty($userIds)) {
                $result = Curl::to(config('config.user_api') . '/api/byIds')->withData([
                    'ids' => $userIds,
                    'apiSecret' => config('config.user_secret')
                ])->post();
                $response = json_decode($result);
                Log::info('ParicipationController-用户信息 : ' . json_encode($result));
                if ($response && $response->data) {
                    foreach ($response->data as $val) {
                        $check[$val->id] = [
                            'name' => $val->username,
                            'userno' => $val->userno
                        ];
                    }
                }
            }

            if ($check) {
                foreach ($datas as $key => $val) {
                    if (isset($check[$val['userId']])) {
                        $datas[$key]['username'] = $check[$val['userId']]['name'];
                        $datas[$key]['userNo'] = $check[$val['userId']]['userno'];
                    }
                }
            }

            return $this->successResponse([
                'list' => $datas,
                'count' => $count,
                'totalPage' => $pagenation['total']
            ]);
        }

        return $this->successNullResponse();
    }

    /**
     * 2.2 报名参与活动 (token)
     *
     * @bodyParam activeId int required 活动ID.
     * @bodyParam userId int required 用户ID.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add(Request $request)
    {
        if (!$this->userInfo) {
            return $this->errorResponse('请先登录');
        }

        //TODO 判断用户是否存在
        $this->validate($request, ParicipationList::$_validRule, ParicipationList::$_messages);

        $data = $request->all();

        //TODO 判断活动状态
        $now = date('Y-m-d H:i:s');
        $active = Actives::where([
            'id' => $data['activeId'],
            'status' => 1
        ])->where('beginTime', '<=', $now)
        ->where('endTime', '>=', $now)
        ->first();
        if ($active) {
            //TODO 判断是否重复报名
            $count = ParicipationList::where([
                'activeId' => $data['activeId'],
                'userId' => $this->userInfo->id //$data['userId']
            ])->count();
            if ($count > 0) {
                return $this->errorResponse('请勿重复报名');
            }

            //TODO 判断活动人数是否超过上限
            if ($active->persons >= intval($active->actual_persons + 1)) {
                DB::beginTransaction();
                $data['chance'] = $active->chance;
                $prizes = ParicipationList::create($data);
                if ($prizes) {
                    $result = $active->update([
                        'actual_persons' => $this->__persoons($data['activeId'])
                    ]);
                    if ($result) {
                        DB::commit();
                        return $this->successResponse('', '报名成功');
                    }
                }
                DB::rollBack();
                return $this->errorResponse('报名添加失败');
            }
            return $this->errorResponse('活动参与人数超过上限');
        }

        return $this->errorResponse('未找到当前活动');
    }

    //退出活动 (token)
    public function del(Request $request)
    {
        if (!$this->userInfo) {
            return $this->errorResponse('请先登录');
        }

        $activeId = $request->input('activeId');

        //$userId = $request->input('userId');
        $userId = $this->userInfo->id;

        if (intval($activeId) && intval($userId) > 0) {
            DB::beginTransaction();
            //TODO 判断用户是否存在
            $active = ParicipationList::where([
                'activeId' => $activeId,
                'userId' => $userId
            ])->delete();

            if ($active) {
                //更新实际参与人数
                $result = Actives::where([
                    'activeId' => $activeId,
                    'status' => 1
                ])->update([
                    'actual_persons' => $this->__persoons($activeId)
                ]);
                if ($result) {
                    DB::commit();
                    return $this->successResponse('', '退出报名成功');
                }
            }
            DB::rollBack();
        }

        return $this->errorResponse('缺少参数');
    }

    /**
     * 2.3 报名信息 (API - 内部使用)
     *
     * @bodyParam userId int required 用户ID.
     * @bodyParam activeId int required 活动ID.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function apiInfo(Request $request)
    {
        $userId = $request->input('userId', 0);
        $activeId = $request->input('activeId', 0);
        if (intval($userId) > 0 && intval($activeId) > 0) {
            $chance = ParicipationList::where([
                'userId' => $userId,
                'activeId' => $activeId,
                'status' => 1
            ])->where('created_at', '<=', nowDate())
            ->sum('chance');
            return $this->successResponse($chance);
        }

        return $this->successResponse(0);
    }

    /**
     * 2.4 报名参与活动 (API - 内部使用)
     *
     * @bodyParam activeId int required 活动ID.
     * @bodyParam userId int required 用户ID.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function apiAdd(Request $request)
    {
        //TODO 判断用户是否存在
        $this->validate($request, ParicipationList::$_validRule, ParicipationList::$_messages);

        $data = $request->all();

        //TODO 判断活动状态
        $now = date('Y-m-d H:i:s');
        $active = Actives::where([
            'id' => $data['activeId'],
            'status' => 1
        ])->where('beginTime', '<=', $now)
            ->where('endTime', '>=', $now)
            ->first();
        if ($active) {
            //TODO 判断是否重复报名
            $count = ParicipationList::where([
                'activeId' => $data['activeId'],
                'userId' => $data['userId']
            ])->count();
            if ($count > 0) {
                return $this->errorResponse('请勿重复报名');
            }

            //TODO 判断活动人数是否超过上限
            if ($active->persons >= intval($active->actual_persons + 1)) {
                DB::beginTransaction();
                $data['chance'] = $active->chance;
                $prizes = ParicipationList::create($data);
                if ($prizes) {
                    $result = $active->update([
                        'actual_persons' => $this->__persoons($data['activeId'])
                    ]);
                    if ($result) {
                        DB::commit();
                        return $this->successResponse('', '报名成功');
                    }
                }
                DB::rollBack();
                return $this->errorResponse('报名添加失败');
            }
            return $this->errorResponse('活动参与人数超过上限');
        }

        return $this->errorResponse('未找到当前活动');
    }

    /**
     * 2.5 报名参与活动 (API - 后台)
     *
     * @bodyParam activeId int required 活动ID.
     * @bodyParam userId int required 用户ID.
     * @bodyParam status int required 状态 0无效 1有效.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus(Request $request)
    {
        $this->validate($request, ParicipationList::$_validRule, ParicipationList::$_messages);

        $data = $request->all();

        $status = $request->input('status', '');

        if (is_numeric($status)) {
            $now = date('Y-m-d H:i:s');
            DB::beginTransaction();
            $active = Actives::where([
                'id' => $data['activeId'],
                'status' => 1
            ])->where('beginTime', '<=', $now)
                ->where('endTime', '>=', $now)
                ->lockForUpdate()
                ->first();
            if ($status > 0) {
                //TODO 判断活动人数是否超过上限
                Log::info('报名实际人数' . ($active->actual_persons));
                if ($active->persons < intval($active->actual_persons + 1)) {
                    return $this->errorResponse('活动参与人数超过上限');
                }
            }

            //TODO 更新用户状态
            $result = Curl::to(config('config.user_api') . 'api/changeStatus')->withData([
                'userId' => $data['userId'],
                'status' => $status,
                'apiSecret' => config('config.user_secret')
            ])->post();
            $response = json_decode($result);
            if ($response->data) {
                if (isset($response->code) && $response->code != 0) {
                    return $this->errorResponse('修改失败');
                }
            }

            if (intval($status) > 0) {
                //TODO 判断是否重复报名
                $count = ParicipationList::where([
                    'activeId' => $data['activeId'],
                    'userId' => $data['userId']
                ])->count();

                if ($count > 0) {
                    //进行修改
                    //DB::beginTransaction();
                    $paricipation = ParicipationList::where([
                        'activeId' => $data['activeId'],
                        'userId' => $data['userId']
                    ])->update([
                        'chance' => $active->chance,
                        'status' => 1
                    ]);
                    if ($paricipation) {
                        $result = $active->update([
                            'actual_persons' => $this->__persoons($data['activeId'])
                        ]);
                        if ($result) {
                            DB::commit();
                            return $this->successResponse('', '报名成功');
                        }
                        return $this->successResponse('', '报名修改成功');
                    }
                    DB::rollBack();
                    return $this->errorResponse('报名修改失败');
                }

                //TODO 判断活动人数是否超过上限
                //DB::beginTransaction();
                $data['chance'] = $active->chance;
                $paricipation = ParicipationList::create($data);
                if ($paricipation) {
                    $result = $active->update([
                        'actual_persons' => $this->__persoons($data['activeId'])
                    ]);
                    if ($result) {
                        DB::commit();
                        return $this->successResponse('', '报名成功');
                    }
                }
                DB::rollBack();
                return $this->errorResponse('报名添加失败');
            } else {
                //TODO 判断是否已经报名
                $count = ParicipationList::where([
                    'activeId' => $data['activeId'],
                    'userId' => $data['userId']
                ])->count();
                if ($count > 0) {
                    //清楚报名信息
                    //DB::beginTransaction();
                    $paricipation = ParicipationList::where([
                        'activeId' => $data['activeId'],
                        'userId' => $data['userId']
                    ])->update([
                        'chance' => 0,
                        'status' => 0
                    ]);
                    if ($paricipation) {
                        $result = $active->update([
                            'actual_persons' => $this->__persoons($data['activeId'])
                        ]);
                        if ($result) {
                            DB::commit();
                            return $this->successResponse('', '修改成功');
                        }
                    }
                    DB::rollBack();
                }
                return $this->successResponse([], '请求成功，但无修改');
            }
        }

        return $this->errorResponse('参数校验失败');
    }

    /**
     * 2.6 查询用户状态 (API - 后台)
     *
     * @bodyParam ids string required 用户ID.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function byIds(Request $request)
    {
        $ids =  $request->input('ids', '');

        if (!empty($ids)) {
            $data = ParicipationList::select('userId')->whereIn('userId', $ids)->where('status', 1)->get();
            return $this->successResponse($data, '获取成功');
        }

        return $this->successNullResponse();
    }

    /**
     * 2.7 重置抽奖机会 (API - 后台)
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function resetChance()
    {
        WinnerRecord::query()->truncate();
        ParicipationList::query()->truncate();
        Actives::query()->update([
            'actual_persons' => 0
        ]);
        return $this->successResponse([], '操作成功');
    }

    private function __persoons($activeId)
    {
        return ParicipationList::where([
            'activeId' => $activeId,
            'status' => 1
        ])->count();
    }

}
