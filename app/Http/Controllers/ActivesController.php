<?php

namespace App\Http\Controllers;

use App\Models\Actives;
use App\Models\ActivesPrizes;
use App\Models\Prizes;
use App\Models\WinnerRecord;
use GatewayClient\Gateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

/**
 * @group 1.活动
 *
 * Class ActivesController
 *
 * @package App\Http\Controllers
 */
class ActivesController extends Controller
{
    /**
     * 1.1 最新活动
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *           "activeTitle" : "活动标题",
     *           "banners" : "活动图片",
     *           "status" : "活动状态",
     *           "persons" : "可参与人数",
     *           "actual_persons" : "实际参与人数",
     *           "description" : "奖品描述",
     *           "beginTime" : "开始时间",
     *           "endTime" : "结束时间",
     *           "timeDays" : "抽奖指定日期",
     *           "prize" : {
     *               "id": "奖品ID",
     *               "aid": "活动奖品ID",
     *               "name": "奖品名称",
     *               "pic": "奖品图片",
     *               "quantity": "奖品数量"
     *              }
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function nowActive()
    {
        $data = $this->_pushNowActive();
        return $this->successResponse($data);
    }

    /**
     * 1.2 活动列表
     *
     * @urlParam status int 活动状态.
     * @urlParam title string 活动标题.
     * @urlParam beginTime string 开始时间.
     * @urlParam endTime string 结束时间.
     * @urlParam page int 当前页.
     * @urlParam pageSize int 每页显示条数.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *           "activeTitle" : "活动标题",
     *           "banners" : "活动图片",
     *           "status" : "活动状态",
     *           "persons" : "可参与人数",
     *           "actual_persons" : "实际参与人数",
     *           "description" : "奖品描述",
     *           "beginTime" : "开始时间",
     *           "endTime" : "结束时间",
     *           "timeDays" : "抽奖指定日期",
     *           "prize" : {
     *               "id": "奖品ID",
     *               "name": "奖品名称",
     *               "pic": "奖品图片",
     *               "quantity": "奖品数量"
     *              }
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $status = $request->get('status', '');

        $beginTime = $request->input('beginTime', '');

        $endTime = $request->input('endTime', '');

        $title = $request->input('title', '');

        //分页参数
        $nowPage = $request->get('page', config('config.page'));

        $pageSize = $request->get('pageSize', config('config.pageSize'));

        $query = Actives::query();

        if (is_numeric($status)) {
            $query->where('status', '=', intval($status));
        }

        if ($title) {
            $query->where('activeTitle', $title);
        }

        $query->where('status', $status);

        if ($beginTime && $endTime) {
            if (strtotime($endTime) > strtotime($beginTime)) {
                $query->where('beginTime', '<=', $beginTime);
                $query->where('endTime', '>=', $endTime);
            }
        }
        $count = $query->count();

        if ($count > 0) {
            $pagenation = getTotalPage($count, (int)$nowPage, (int)$pageSize);
            //->select('id', 'activeTitle', 'banners', 'actual_persons', 'description', 'beginTime', 'endTime')
            $list = $query
            ->offset($pagenation['page'])
            ->limit($pagenation['limit'])
            ->get();

            $datas = $list->toArray();
            foreach ($list as $key => $val) {
                $prizes = $val->prizes;
                foreach ($prizes as $p) {
                    $pinfo = $p->prizeInfo;
                    $datas[$key]['prize'][] = [
                        'id' => $p->id,
                        'name' => $pinfo->name,
                        'pic' => $pinfo->imgs,
                        'quantity' => $p->quantity
                    ];
                }
            }

            return $this->successResponse([
                'list' => $datas,
                'count' => $count,
                'totalPage' => $pagenation['total']
            ]);
        }

        return $this->successNullResponse();
    }

    /**
     * 1.3 添加活动
     *
     * @bodyParam activeTitle string required 奖品名称.
     * @bodyParam chance int required 抽奖次数.
     * @bodyParam description string 描述.
     * @bodyParam persons int required 参与人数.
     * @bodyParam beginTime date required 活动开始时间.
     * @bodyParam endTime date required 活动结束时间.
     * @bodyParam timeDays string required 指定抽奖日期.
     * @bodyParam timeHour string required 指定抽奖小时.
     * @bodyParam timeMinutes string required 指定抽奖分钟.
     * @bodyParam banners string 活动图片.
     *
     * @response {
     *       "message": "活动创建成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add(Request $request)
    {
        $this->validate($request, Actives::$_validRule, Actives::$_messages);

        $data = $request->all();

        //判断开始时间是否大于结束时间
        if (strtotime($data['endTime']) <= strtotime($data['beginTime'])) {
            return $this->errorResponse('请检查开始时间与结束时间');
        }

        //修改时间格式
        $data['beginTime'] = date('Y-m-d 00:00:00', strtotime($data['beginTime']));
        $data['endTime'] = date('Y-m-d 23:59:59', strtotime($data['endTime']));

        //检查指定日期
        if (strpos($data['timeDays'], ';') !== false || is_numeric($data['timeDays'])) {
            $days = explode(';', $data['timeDays']);
            foreach ($days as $val) {
                if (!is_numeric($val)) {
                    return $this->errorResponse('指定日期格式不正确');
                }
            }
        } else {
            return $this->errorResponse('指定日期格式不正确');
        }

        $actives = Actives::create($data);

        if ($actives) {
            return $this->successResponse(['id' => $actives->id], '活动创建成功');
        }

        return $this->errorResponse('活动创建失败');
    }

    /**
     * 1.4 添加活动奖品
     *
     * @bodyParam activiteId string required 活动ID.
     * @bodyParam prizeId int required 奖品ID.
     * @bodyParam quantity int required 奖品数量.
     *
     * @response {
     *       "message": "活动奖品添加成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addActivePrize(Request $request)
    {
        $activeId = $request->input('activiteId');

        $aprizeId = $request->input('prizeId');

        $quantity = $request->input('quantity', 0);

        if (is_numeric($activeId) && is_numeric($aprizeId)) {
            if($quantity <= 0) {
                return $this->errorResponse('请填写奖品数量');
            }

            $actives = Actives::where(['id' => $activeId])->first();
            if (!$actives) {
                return $this->errorResponse('获取活动失败');
            }

            $prize = Prizes::where(['id' => $aprizeId, 'status' => 1])->first();
            if (!$prize) {
                return $this->errorResponse('获取奖品失败');
            }

            //TODO 判断奖品数量是否超过了库存
            if ($prize->stock < $quantity) {
                return $this->errorResponse('奖品库存不足');
            }

            //判断奖品是否已经添加过了
            $ap = ActivesPrizes::where([
                'activiteId' => $activeId,
                'prizeId' => $aprizeId
            ])->count();
            if ($ap <= 0) {
                $activePrize = ActivesPrizes::create([
                    'activiteId' => $activeId,
                    'prizeId' => $aprizeId,
                    'quantity' => $quantity
                ]);
                if ($activePrize) {
                    $pushActive = $this->_pushNowActive();
                    try {
                        Log::info('添加活动奖品推送');
                        $res = json_encode([
                            'code' => 100,
                            'msg' => '活动详情',
                            'data' => $pushActive
                        ]);
                        Gateway::$registerAddress = '127.0.0.1:12360';
                        Gateway::sendToAll($res);
                    } catch (\Exception $e) {
                        Log::info('添加活动奖品推送， 异常:' . $e->getMessage());
                    }
                    return $this->successResponse('', '活动奖品添加成功');
                }
            }
            return $this->errorResponse('请勿重复添加活动奖品');
        }

        return $this->errorResponse('活动奖品添加失败');
    }

    /**
     * 1.5 删除活动奖品
     *
     * @bodyParam activiteId string required 活动ID.
     * @bodyParam prizeId int required 奖品ID.
     *
     * @response {
     *       "message": "活动奖品删除成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function delActivePrize(Request $request)
    {
        $activeId = $request->input('activiteId');

        $aprizeId = $request->input('prizeId');

        if (is_numeric($activeId) && is_numeric($aprizeId)) {
            $delete = ActivesPrizes::where([
                'activiteId' => $activeId,
                'prizeId' => $aprizeId
            ])->delete();
            if ($delete) {
                $pushActive = $this->_pushNowActive();
                try {
                    Log::info('删除活动奖品推送');
                    $res = json_encode([
                        'code' => 100,
                        'msg' => '活动详情',
                        'data' => $pushActive
                    ]);
                    Gateway::$registerAddress = '127.0.0.1:12360';
                    Gateway::sendToAll($res);
                } catch (\Exception $e) {
                    Log::info('删除活动奖品推送， 异常:' . $e->getMessage());
                }
                return $this->successResponse('', '活动奖品删除成功');
            }
        }

        return $this->errorResponse('活动奖品删除失败');
    }

    /**
     * 1.6 修改活动信息
     *
     * @bodyParam id int required 活动ID.
     * @bodyParam activeTitle string required 奖品名称.
     * @bodyParam chance int required 抽奖次数.
     * @bodyParam description string 描述.
     * @bodyParam persons int required 参与人数.
     * @bodyParam beginTime date required 活动开始时间.
     * @bodyParam endTime date required 活动结束时间.
     * @bodyParam timeDays string required 指定抽奖日期.
     * @bodyParam timeHour string required 指定抽奖小时.
     * @bodyParam timeMinutes string required 指定抽奖分钟.
     * @bodyParam banners string 活动图片.
     *
     * @response {
     *       "message": "活动修改成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit(Request $request)
    {
        $id = $request->get('id', '');

        if (intval($id) > 0) {
            $active = Actives::where('id', $id)->first();

            if ($active) {
                Actives::$_validRule['activeTitle'] = Actives::$_validRule['activeTitle'] . ',activeTitle,' . $id;
                $this->validate($request, Actives::$_validRule, Actives::$_messages);
                $data = $request->all();
                unset($data['id']);

                //检查指定日期
                if (strpos($data['timeDays'], ';') !== false || is_numeric($data['timeDays'])) {
                    $days = explode(';', $data['timeDays']);
                    foreach ($days as $val) {
                        if (!is_numeric($val)) {
                            return $this->errorResponse('指定日期格式不正确');
                        }
                    }
                } else {
                    return $this->errorResponse('指定日期格式不正确');
                }

                $prizes = $active->where('id', $id)->update($data);
                if ($prizes) {
                    $pushActive = $this->_pushNowActive();
                    try {
                        Log::info('修改活动信息推送');
                        $res = json_encode([
                            'code' => 100,
                            'msg' => '活动详情',
                            'data' => $pushActive
                        ]);
                        Gateway::$registerAddress = '127.0.0.1:12360';
                        Gateway::sendToAll($res);
                    } catch (\Exception $e) {
                        Log::info('修改活动信息推送， 异常:' . $e->getMessage());
                    }
                    return $this->successResponse('活动信息修改成功');
                }
                return $this->errorResponse('活动信息修改失败');
            }
            return $this->errorResponse('未找到活动记录');
        }

        return $this->errorResponse('缺少参数');
    }

    /**
     * 1.7 删除活动
     *
     * @bodyParam id int required 活动ID.
     *
     * @response {
     *       "message": "活动删除成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function del(Request $request)
    {
        $id = $request->get('id', '');

        if (intval($id) > 0) {
            $actives = Actives::where('id', $id)->first();
            if ($actives) {
                $delete = $actives->delete();
                if ($delete) {
                    return $this->successResponse('', '活动删除成功');
                }
            }
            return $this->errorResponse('未找到活动记录');
        }

        return $this->errorResponse('缺少参数');
    }

    /**
     * 1.8 最新活动 (API - 内部使用)
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiList()
    {
        $data = [];
        $now = date('Y-m-d H:i:s');
        $active = Actives::select('id', 'activeTitle')
            ->where(['status' => 1])
            ->where('beginTime', '<=', $now)
            ->where('endTime', '>=', $now)
            ->first();
        if ($active) {
            $data = $active->toArray();
        }
        return $this->successResponse($data);
    }

    /**
     * 1.9 修改活动奖品
     *
     * @bodyParam id string required 活动奖品ID.
     * @bodyParam quantity int required 奖品数量.
     *
     * @response {
     *       "message": "活动奖品添加成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function editActivePrize(Request $request)
    {
        $id = $request->input('id');

        $quantity = $request->input('quantity', 0);

        if (is_numeric($id) && is_numeric($quantity)) {
            if($quantity <= 0) {
                return $this->errorResponse('请填写奖品数量');
            }

            $aprize = ActivesPrizes::where(['id' => $id])->first();

            if (!$aprize) {
                return $this->errorResponse('获取活动奖品失败');
            }

            //TODO 判断奖品数量是否超过了库存
            if ($aprize->prizeInfo->stock < $quantity) {
                return $this->errorResponse('奖品库存不足');
            }

            //判断奖品是否已经添加过了
            $update = $aprize->update([
                'quantity' => $quantity
            ]);

            if ($update) {
                $pushActive = $this->_pushNowActive();
                try {
                    Log::info('修改活动奖品推送');
                    $res = json_encode([
                        'code' => 100,
                        'msg' => '活动详情',
                        'data' => $pushActive
                    ]);
                    Gateway::$registerAddress = '127.0.0.1:12360';
                    Gateway::sendToAll($res);
                } catch (\Exception $e) {
                    Log::info('修改活动奖品信息， 异常:' . $e->getMessage());
                }
                return $this->successResponse('', '活动奖品修改成功');
            }

        }

        return $this->errorResponse('活动奖品修改失败');
    }

    private function _pushNowActive()
    {
        $data = [];

//        $now = date('Y-m-d H:i:s');
//          select('id', 'activeTitle', 'banners', 'persons', 'actual_persons', 'chance', 'description', 'beginTime', 'endTime', 'timeDays')->
        $active = Actives::where(['status' => 1])
//            ->where('beginTime', '<=', $now)
//            ->where('endTime', '>=', $now)
            ->first();

        if ($active) {
            $data = $active->toArray();
            $data['prize'] = [];
            $data['persons'] = 0;
            $prizes = $active->prizes;
            if (!empty($prizes)) {
                foreach ($prizes as $p) {
                    $pinfo = $p->prizeInfo;
                    //$prize = [];
                    //if ($p->quantity > 0) {
                        $prize = [
                            'id' => $pinfo->id,
                            'aid' => $p->id,
                            'name' => $pinfo->name,
                            'pic' => $pinfo->imgs,
                            'quantity' => $p->quantity
                        ];
                    //}
                    $data['prize'][] = $prize;
                }
            }

            $begin = date('Y-m-01 00:00:00');
            $end = date('Y-m-d 23:59:59');
            $winnerRecord = WinnerRecord::whereBetween('created_at', [$begin, $end])
                ->select('userId')
                ->get();
            $notIn = [];
            if (!$winnerRecord->isEmpty()) {
                foreach ($winnerRecord as $wr) {
                    $notIn[] = $wr->userId;
                }
            }

            //获取所有用户
            $result = Curl::to(config('config.user_api') . '/api/allUsers')->withData([
                'notIn' => $notIn,
                'apiSecret' => config('config.user_secret')
            ])->post();
            $response = json_decode($result);
            //Log::info('ActivesController-用户信息 : ' . json_encode($result));
            if ($response && $response->data) {
                $data['persons'] = count((array)$response->data);
            }
        }

        return $data;
    }

}
