<?php

namespace App\Http\Controllers;

use App\Models\Actives;
use App\Models\ActivesPrizes;
use App\Models\ParicipationList;
use App\Models\Prizes;
use App\Models\WinnerRecord;
use App\Services\RandCalculationServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

/**
 * @group 4.中奖
 *
 * Class WinnerController
 *
 * @package App\Http\Controllers
 */
class WinnerController extends Controller
{

    /**
     * 4.1 中奖记录列表
     *
     * @urlParam activiteId int 活动ID.
     * @urlParam userId int 用户ID.
     * @urlParam deptId int 部门ID.
     * @urlParam date date 查询时间.
     * @urlParam page int 当前第几页.
     * @urlParam pageSize int 每页显示条数.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *          "activeTitle": "活动标题",
     *          "prizeName": "奖品名称",
     *          "userId": "用户ID",
     *          "username": "用户名"
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $activiteId  = $request->input('activiteId', '');

        $userId = $request->input('userId', '');

        $deptId = $request->input('deptId', '');

        //分页参数
        $nowPage = $request->get('page', config('config.page'));

        $pageSize = $request->get('pageSize', config('config.pageSize'));

        $query = WinnerRecord::query();

        if (intval($activiteId) > 0) {
            $query->where('activiteId', $activiteId);
        }

        if (intval($userId) > 0) {
            $query->where('userId', $userId);
        }

        if (intval($deptId) > 0) {
            $query->where('deptId', $deptId);
        }

        $date = $request->input('date', '');
        if ($date) {
            if (strtotime($date) !== false) {
                $begin = date('Y-m-d 00:00:00', strtotime($date));
                $end = date('Y-m-d 23:59:59', strtotime($date));
                $query->whereBetween('created_at', [$begin, $end]);
            }
        } else {
            //查询最近一期的中奖列表
            $created_at = WinnerRecord::select('created_at')->orderBy('created_at', 'desc')->first();
            if ($created_at) {
                $begin = date('Y-m-d 00:00:00', strtotime($created_at->created_at));
                $end = date('Y-m-d H:i:s', strtotime($created_at->created_at));
                $query->whereBetween('created_at', [$begin, $end]);
            }
        }

        $count = $query->count();

        if ($count > 0) {
            $pagenation = getTotalPage($count, (int)$nowPage, (int)$pageSize);
            $list = $query
                ->offset($pagenation['page'])
                ->limit($pagenation['limit'])
                ->get();

            $datas = [];
            $userIds = $check = [];
            foreach ($list as $key => $val) {
                $userIds[] = $val['userId'];
                $datas[$key] = [
                    'activeTitle' => $val->activeInfo->activeTitle,
                    'prizeName' => $val->name,
                    'userId' => $val->userId,
                    'username' => '无名氏',
                    'created_at' => date('Y-m-d', strtotime($val->created_at))
                ];
            }

            if (!empty($userIds)) {
                $result = Curl::to(config('config.user_api') . '/api/byIds')->withData([
                    'ids' => $userIds,
                    'apiSecret' => config('config.user_secret')
                ])->post();
                $response = json_decode($result);
                //Log::info('WinnerController-用户信息 : ' . json_encode($result));
                if ($response && $response->data) {
                    foreach ($response->data as $val) {
                        $check[$val->id] = [
                            'uname' => $val->username,
                            'userno' => $val->userno
                        ];
                    }
                }
            }

            if ($check) {
                foreach ($datas as $key => $val) {
                    if (isset($check[$val['userId']])) {
                        $datas[$key]['username'] = $check[$val['userId']]['uname'];
                        $datas[$key]['userno'] = $check[$val['userId']]['userno'];
                    }
                }
            }

            return $this->successResponse([
                'list' => $datas,
                'count' => $count,
                'totalPage' => $pagenation['total']
            ]);
        }

        return $this->successNullResponse();
    }

    /**
     * 4.2 抽奖 (token)
     *
     * @bodyParam activiteId int required 活动ID.
     * @bodyParam userId int required 用户ID.
     *
     * @response {
     *       "message": "恭喜中奖 / 重在参与",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function lottery(Request $request)
    {
        if (!$this->userInfo) {
            return $this->errorResponse('请先登录');
        }

        $activiteId  = $request->input('activiteId', '');

        if (empty($activiteId)) {
            return $this->errorResponse('缺少活动ID');
        }

        //TODO 判断用户是否存在
//        $userId = $request->input('userId', 0);
//
//        if (intval($userId) <= 0) {
//            return $this->errorResponse('请传入用户ID');
//        }

        $userId = $this->userInfo->id;

        //防止重复请求

        //获取活动状态
        $now = date('Y-m-d H:i:s');
        $query = Actives::query();
        $query->where('id', $activiteId);
        $query->where('status', 1);
        $query->where('beginTime', '<=', $now);
        $query->where('endTime', '>=', $now);
        $active = $query->first();

        if ($active) {
            //判断活动是否是指定日期
            $timeDays = $active->timeDays;
            if (empty($timeDays)) {
                return $this->errorResponse('活动未开始');
            }

            $dayCheck = false;
            $timeDays = explode(';', $timeDays);
            $day = date('Y-m-d');
            foreach ($timeDays as $val) {
                if ($day == date('Y-m-'.str_pad($val, 2, '0', STR_PAD_LEFT))) {
                    $dayCheck = true;
                    break;
                }
            }

            if (!$dayCheck) {
                return $this->errorResponse('客官别着急，抽奖暂未开始，抽奖时间为每月的' . implode('、', $timeDays) . '号');
            }

            $persons = $active->persons;//获取参与人数
            $actual_persons = $active->actual_persons;//实际参与人数
            //再次判断参与人数与实际参与人数
            if ($persons < $actual_persons) {
                return $this->errorResponse('出现异常，请联系管理员');
            }

            $prizes = $active->prizes;

            if ($prizes->count() <= 0) {
                return $this->errorResponse('当前活动没有设置奖品');
            }

            //TODO 判断用户是否还有抽奖次数
            $record = ParicipationList::where([
                'activeId' => $activiteId,
                'userId' => $userId,
                'status' => 1
            ])->first();
            if (!isset($record) || $record->chance <= 0) {
                return $this->errorResponse('您还没有抽奖次数');
            };

            //TODO 判断是否已经中奖
            $isRecord = WinnerRecord::where([
                'activiteId' => $activiteId,
                'userId' => $userId
            ])
                //加上时间做判断
                ->whereBetween('created_at', [date('Y-m-d 00:00:00'), date('Y-m-d H:i:s')])
                ->count();
            if($isRecord > 0) {
                $record->update([
                    'chance'=> 0
                ]);
                //return $this->errorResponse('如此贪心！中奖了还来？');
                return $this->errorResponse('重在参与');
            }

            //获取活动奖品 - 列表
            $prizesArr = [];
            $prizeCount = 0;
            foreach ($prizes as $p) {
                if ($p->quantity > 0) {
                    $prizeCount += intval($p->quantity);
                    $prizesArr[] = [
                        'id' => $p->prizeId,
                        'name' => $p->prizeInfo->name,
                        'num' => $p->quantity,
                        'level'=> $p->prizeInfo->level
                    ];
                }
            }
            if ($prizeCount <= 0) {
                $this->_noReward($record, $activiteId, $userId);
                return $this->successResponse([], '重在参与');
            }
            Log::info('奖品列表 ' . json_encode($prizesArr));

            //TODO 获取总共抽奖剩余次数
            $chance = ParicipationList::where([
                'activeId' => $activiteId,
                'status' => 1
            ])->sum('chance');
            Log::info('剩余机会 ' . json_encode($chance));

            //TODO 开始抽奖
            $RandCalculationServices = new RandCalculationServices();
            $reward = $RandCalculationServices->reward($prizesArr, $chance);
            if (!$reward) {
                return $this->errorResponse('抽奖出现异常');
            }

            if ($reward['id'] > 0) {
                DB::beginTransaction();
                //TODO 增加中奖记录
                $winnerRecord = WinnerRecord::create([
                    'activiteId' => $activiteId,
                    'userId' => $userId,
                    'prizeId' => $reward['id'],
                    'name' => $reward['name'],
                    'level' => $reward['level'],
                    'status' => 1
                ]);

                Log::info('更新信息 winnerRecord ' . json_encode($winnerRecord));
                if (!$winnerRecord) {
                    Log::info('更新信息回滚 winnerRecord ' . json_encode($winnerRecord));
                    DB::rollBack();
                    return $this->successResponse([], '重在参与');
                }

                //TODO 机会直接清 0,
                $record->prizeId = $reward['id'];
                $record->isReward = 1;
                $record->chance = ($record->chance - 1) >= 0 ? ($record->chance - 1) : 0;
                Log::info('更新信息 record ' . json_encode($record));
                $update = $record->update();
                Log::info('更新信息 update ' . json_encode($update));
                if (!$update) {
                    Log::info('更新信息回滚 update ' . json_encode($update));
                    DB::rollBack();
                    return $this->successResponse([], '重在参与');
                }

                //TODO 更新奖品数量
                $quantity = ActivesPrizes::where([
                    'activiteId' => $activiteId,
                    'prizeId' => $reward['id']
                ])->decrement('quantity');
                Log::info('更新信息quantity ' . json_encode($quantity));
                if (!$quantity) {
                    Log::info('更新信息回滚quantity ' . json_encode($quantity));
                    DB::rollBack();
                    return $this->successResponse([], '重在参与');
                }

                DB::commit();
                return $this->successResponse($reward, '恭喜中奖');
            } else {
                $this->_noReward($record, $activiteId, $userId);
            }

            return $this->successResponse([], '重在参与');
        }

        return $this->errorResponse('未找到对应的活动');
    }

    /**
     * 4.3 中奖记录列表 (API - 内部使用)
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiList()
    {
        $query = WinnerRecord::query();

        $query->where('status', 1);

        $query->orderBy('created_at', 'desc');

        $query->orderBy('level', 'asc');

        $count = $query->count();

        if ($count > 0) {
            $list = $query->select('name', 'level', 'created_at')->get();

            $datas = [];
            foreach ($list as $key => $val) {
                if (isset(Prizes::level[$val->level])) {
                    $datas[$key] = date('Y-m-d', strtotime($val->created_at)) . " asdasd" . " 中". Prizes::level[$val->level] . ' '. $val->name;
                }
            }
            return $this->successResponse($datas);
        }

        return $this->successNullResponse();
    }

    /**
     * 4.4 当前用户中奖记录
     *
     * @bodyParam activeId int required 活动ID.
     *
     * @response {
     *       "message": "处理成功",
     *       "code":0,
     *       "data": {
     *             "prizeId" : "奖品ID",
     *             "name" : "奖品名称",
     *             "level" : "奖品等级"
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userInfo(Request $request)
    {
        if (!$this->userInfo) {
            return $this->errorResponse('请先登录');
        }

        $activiteId  = $request->input('activeId', '');

        $query = WinnerRecord::query();

        if (is_numeric($activiteId)) {
            $data = $query
                ->select('prizeId', 'name', 'level')
                ->where([
                    'activiteId' => intval($activiteId),
                    'userId' => $this->userInfo->id,
                    'status' => 1
                ])
                ->first();

            if ($data) {
                $rdata = $data->toArray();
                $rdata['prize_pic'] = isset($data->prize->imgs) ? $data->prize->imgs : '';
                return $this->successResponse($rdata, '中奖记录');
            }
        }

        return $this->successNullResponse();
    }

    /**
     * 4.5 获取用户当前状态
     *
     * @bodyParam userId int required 用户ID.
     *
     * @response {
     *       "message": "可以清除",
     *       "code":0,
     *       "data": {
     *       }
     *   }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delUser(Request $request)
    {
        $userId = $request->input('userId', 0);
        if (intval($userId) > 0) {
            $wcount = WinnerRecord::where('userId', $userId)->count();
            if ($wcount > 0) {
                return $this->errorResponse('该用户已经有中奖记录，无法删除');
            }
            $puser = ParicipationList::where('userId', $userId)->first();
            if ($puser) {
                if ($puser->status == 1) {
                    return $this->errorResponse('该用户已经报名参与活动，请先取消');
                }
            }
            return $this->successResponse([], '可以清除');
        }
        return $this->errorResponse('缺少参数');
    }

    private function _noReward($record, $activiteId, $userId)
    {
        DB::beginTransaction();
        //TODO 未中奖则减少一次机会
        $update = $record->decrement('chance');
        if (!$update) {
            DB::rollBack();
            Log::info('未中奖信息回滚 ' . json_encode($update));
        }

        //增加一次抽奖记录 - 暂时写死。
        $winnerRecord = WinnerRecord::create([
            'activiteId' => $activiteId,
            'userId' => $userId,
            'prizeId' => 4,
            'name' => '感谢参与',
            'level' => 4,
            'status' => 1
        ]);

        if (!$winnerRecord) {
            Log::info('未中奖记录插入失败 ' . json_encode($winnerRecord));
            DB::rollBack();
        }
        DB::commit();
    }

}
