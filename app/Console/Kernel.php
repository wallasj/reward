<?php

namespace App\Console;

use App\Console\Commands\AutoListServer;
use App\Console\Commands\AutoRewardServer;
use App\Console\Commands\AutoStatusServer;
use App\Console\Commands\GatewayWorkerServer;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        GatewayWorkerServer::class,
        AutoRewardServer::class,
        AutoStatusServer::class,
        AutoListServer::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
        $schedule->command('auto:reward')->everyMinute();
        $schedule->command('auto:status')->everyMinute();
        //$schedule->command('auto:winnerList')->everyFiveMinutes();
    }
}
