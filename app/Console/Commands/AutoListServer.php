<?php


namespace App\Console\Commands;

use App\Models\WinnerRecord;
use GatewayClient\Gateway;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

class AutoListServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:winnerList';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show newest winner list.';

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
        Gateway::$registerAddress = '127.0.0.1:12360';
    }

    /**
     * Execute the console command.
     *
     * [@return](https://learnku.com/users/31554) mixed
     */
    public function handle()
    {
        Log::info('进入自动脚本-奖品列表模块');
        $data = $this->_rewardList();
        try {
            $res = json_encode([
                'code' => 200,
                'msg' => '中奖列表',
                'data' => $data
            ]);
            Gateway::sendToAll($res);
        } catch (\Exception $e) {
            Log::info('中奖列表， 异常:' . $e->getMessage());
        }
    }

    //返回中奖列表
    private function _rewardList()
    {
        $query = WinnerRecord::query();
        $begin = date('Y-m-d 00:00:00');
        $end = date('Y-m-d 23:59:59');
        $query->whereBetween('created_at', [$begin, $end]);
        $count = $query->count();

        $datas = [];
        if ($count > 0) {
            $list = $query->get();
            $userIds = $check = [];
            foreach ($list as $key => $val) {
                $userIds[] = $val['userId'];
                $datas[$key] = [
                    'activeTitle' => $val->activeInfo->activeTitle,
                    'prizeName' => $val->name,
                    'userId' => $val->userId,
                    'username' => '无名氏',
                    'created_at' => date('Y-m-d', strtotime($val->created_at))
                ];
            }

            if (!empty($userIds)) {
                $result = Curl::to(config('config.user_api') . '/api/byIds')->withData([
                    'ids' => $userIds,
                    'apiSecret' => config('config.user_secret')
                ])->post();
                $response = json_decode($result);
                Log::info('AutoListServer-用户信息 : ' . json_encode($result));
                if ($response && $response->data) {
                    foreach ($response->data as $val) {
                        $check[$val->id] = [
                            'uname' => $val->username,
                            'userno' => $val->userno
                        ];
                    }
                }
            }

            if ($check) {
                foreach ($datas as $key => $val) {
                    if (isset($check[$val['userId']])) {
                        $datas[$key]['userno'] = $check[$val['userId']]['userno'];
                        $datas[$key]['username'] = $check[$val['userId']]['uname'];
                    }
                }
            }
        }

        return $datas;
    }

}
