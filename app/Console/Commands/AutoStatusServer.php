<?php


namespace App\Console\Commands;

use App\Models\Actives;
use App\Models\ActivesPrizes;
use GatewayClient\Gateway;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AutoStatusServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show Now Active Status.';

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
        Gateway::$registerAddress = '127.0.0.1:12360';
    }

    /**
     * Execute the console command.
     *
     * [@return](https://learnku.com/users/31554) mixed
     */
    public function handle()
    {
        Log::info('进入自动脚本-获取状态-执行命令');
        $this->_getStatus();
    }

    private function _getStatus()
    {
        //判断时间是否满足条件
        Log::info('进入自动脚本-获取状态-执行命令');
        $query = Actives::query();
        $query->select('id', 'timeDays', 'timeHour', 'timeMinutes');
        $query->where('status', 1);
        $active = $query->first();
        if ($active) {
            Log::info('进入自动脚本-获取状态-进入活动');
            //检查活动是否开始
            if (nextTime($active->timeDays, $active->timeHour, $active->timeMinutes)) {
                //活动已开始
                $quantity = ActivesPrizes::where('activiteId', $active->id)
                    ->sum('quantity');

                if ($quantity <= 0) {
                    //奖品已抽完
                    try {
                        $res = json_encode([
                            'code' => 500,
                            'msg' => '奖品已抽完'
                        ]);
                        Gateway::sendToAll($res);
                    } catch (\Exception $e) {
                        Log::info('奖品已抽完， 异常:' . $e->getMessage());
                    }
                    return true;
                }

                //抽奖中
                try {
                    $res = json_encode([
                        'code' => 200,
                        'msg' => '活动进行中'
                    ]);
                    Gateway::sendToAll($res);
                } catch (\Exception $e) {
                    Log::info('活动进行中， 异常:' . $e->getMessage());
                }
                return true;
            }

            Log::info('进入自动脚本-获取状态-活动未开始');
            //活动未开始
            try {
                $res = json_encode([
                    'code' => 202,
                    'msg' => '活动未开始'
                ]);
                Gateway::sendToAll($res);
            } catch (\Exception $e) {
                Log::info('活动未开始， 异常:' . $e->getMessage());
            }
            return true;
        }
        return true;
    }

}
