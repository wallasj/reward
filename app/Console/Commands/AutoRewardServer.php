<?php


namespace App\Console\Commands;
use App\Models\Actives;
use App\Models\ActivesPrizes;
use App\Models\WinnerRecord;
use App\Services\FixedCalculationServices;
use GatewayClient\Gateway;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

class AutoRewardServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:reward';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto do reward everyday.';

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
        Gateway::$registerAddress = '127.0.0.1:12360';
    }

    /**
     * Execute the console command.
     *
     * [@return](https://learnku.com/users/31554) mixed
     */
    public function handle()
    {
        Log::info('进入自动脚本-自动抽奖模块');
        $this->_autoReward();
    }

    private function _autoReward()
    {
        //判断时间是否满足条件
        //Log::info('进入自动脚本-自动抽奖模块-执行命令');
        $query = Actives::query();
        $query->select('id', 'timeDays', 'timeHour', 'timeMinutes');
        $query->where('status', 1);
        $active = $query->first();
        if ($active) {
            Log::info('进入自动脚本-自动抽奖模块-进入活动');
            //检查活动是否开始
            if (nextTime($active->timeDays, $active->timeHour, $active->timeMinutes)) {
                //Log::info('进入自动脚本-自动抽奖模块-活动已开始');
                //活动已开始
                $quantity = ActivesPrizes::where('activiteId', $active->id)
                    ->sum('quantity');
                if ($quantity <= 0) {
                    try {
                        $res = json_encode([
                            'code' => 500,
                            'msg' => '当前活动奖品不足'
                        ]);
                        Gateway::sendToAll($res);
                    } catch (\Exception $e) {
                        Log::info('自动抽奖模块-活动奖品不足， 异常:' . $e->getMessage());
                    }
                    //Log::info('自动抽奖模块-活动奖品不足');
                    return false;
                }

                //获取奖品列表
                $prize = ActivesPrizes::where('activiteId', $active->id)
                    ->where('quantity', '>', 0)
                    ->get();
                $prizes = [];
                foreach ($prize as $val) {
                    $_p = $val->prizeInfo;
                    $prizes[] = [
                        'pid' => $_p->id,
                        'pname' => $_p->name,
                        'plevel' => $_p->level,
                        'pnum' => $val->quantity
                    ];
                }

                //判断用户月初到现在是否已经中奖
                $begin = date('Y-m-01 00:00:00');
                $end = date('Y-m-d H:i:s');
                $winnerRecord = WinnerRecord::whereBetween('created_at', [$begin, $end])
                    ->select('userId')
                    ->get();
                $notIn = [];
                if (!$winnerRecord->isEmpty()) {
                    foreach ($winnerRecord as $wr) {
                        $notIn[] = $wr->userId;
                    }
                }

                //获取所有用户
                $users = [];
                $result = Curl::to(config('config.user_api') . '/api/allUsers')->withData([
                    'notIn' => $notIn,
                    'apiSecret' => config('config.user_secret')
                ])->post();
                $response = json_decode($result);
                Log::info('自动抽奖模块-用户信息 : ' . json_encode($result));
                if ($response && $response->data) {
                    foreach ($response->data as $val) {
                        $users[] = [
                            'uid' => $val->id,
                            'uname' => $val->username,
                            'userno' => $val->userno
                        ];
                    }
                }

                if (empty($users)) {
                    try {
                        $res = json_encode([
                            'code' => 500,
                            'msg' => '缺少参与用户'
                        ]);
                        Gateway::sendToAll($res);
                    } catch (\Exception $e) {
                        Log::info('自动抽奖模块-缺少参与用户， 异常:' . $e->getMessage());
                    }
                    Log::info('自动抽奖模块-缺少参与用户');
                    return false;
                }

                if ($prizes && $users) {
                    //执行抽奖
                    $service = new FixedCalculationServices();
                    $result = $service->reward($prizes, $users);

                    try {
                        DB::beginTransaction();
                        //TODO 增加中奖记录
                        $winnerRecord = WinnerRecord::create([
                            'activiteId' => $active->id,
                            'userId' => $result['uid'],
                            'userNo' => $result['userno'],
                            'prizeId' => $result['prize_id'],
                            'name' => $result['prize_name'],
                            'level' => $result['plevel'],
                            'status' => 1
                        ]);

                        Log::info('自动抽奖模块-更新信息 winnerRecord ' . json_encode($winnerRecord));
                        if (!$winnerRecord) {
                            Log::info('自动抽奖模块-更新信息回滚 winnerRecord ' . json_encode($winnerRecord));
                            DB::rollBack();
                            return false;
                        }

                        //减少奖品数量
                        $quantity = ActivesPrizes::where([
                            'activiteId' => $active->id,
                            'prizeId' => $result['prize_id']
                        ])->decrement('quantity');
                        Log::info('自动抽奖模块-更新信息 quantity ' . json_encode($quantity));
                        if (!$quantity) {
                            Log::info('自动抽奖模块-更新信息回滚 quantity');
                            DB::rollBack();
                            return false;
                        }

                        DB::commit();
                        try {
                            Log::info('自动抽奖模块-更新信息推送');
                            $res = json_encode([
                                'code' => 203,
                                'msg' => '恭喜中奖',
                                'data' => $result
                            ]);
                            Gateway::sendToAll($res);
                            //Artisan::call('auto:winnerList');
                        } catch (\Exception $e) {
                            Log::info('自动抽奖模块-恭喜中奖， 异常:' . $e->getMessage());
                        }
                        return true;
                    } catch(\Exception $e) {
                        Log::info('自动抽奖模块-事务异常 update ' . $e->getMessage());
                    }
                }
                Log::info('自动抽奖模块-抽奖失败');
                return false;
            }
            //Log::info('自动抽奖模块-活动未开始');
            return false;
        }
        Log::info('自动抽奖模块-未找到活动');
        return false;
    }

}
