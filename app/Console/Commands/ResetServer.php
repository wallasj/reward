<?php


namespace App\Console\Commands;
use App\Events\GatewayWorkerEvent;
use App\Models\ParicipationList;
use GatewayWorker\BusinessWorker;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ResetServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ResetUserData.';

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * [@return](https://learnku.com/users/31554) mixed
     */
    public function handle()
    {
        //重置用户抽奖机会
        $update = ParicipationList::update([
            'status' => 1,
            'isReward' => 0,
            'chance' => 1
        ]);

        if ($update) {
            Log::info('更新成功' . json_encode($update));
            return true;
        }

        Log::info('更i性能失败');
    }

}
