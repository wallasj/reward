<?php

namespace App\Services;

//固定算法
class FixedCalculationServices {

    /**
     * 根据奖品数量和抽奖人权重,随机抽取奖品
     *
     * @return array
     */
    public function reward(array $prizes = [], array $users = []) {
        //$this->_generateTestData($prizes, $users);

        // 形成奖品 id 和 name 映射,便于返回时,返回直观的奖品名称
        $prize_mapping = [];
        foreach ($prizes as $prize) {
            $prize_mapping[$prize['pid']] = [
                'pname' => $prize['pname'],
                'plevel' => $prize['plevel']
            ];
        }

        // 形成用户 id 和 name 映射,便于返回时,返回直观的用户名称
        $user_mapping = [];
        foreach ($users as $user) {
            $user_mapping[$user['uid']] = [
                'uname' => $user['uname'],
                'userno' => $user['userno']
            ];
        }

        // 根据奖品数量,生成奖池
        $prize_pool = [];
        foreach ($prizes as $prize) {
            $num        = empty($prize['pnum']) ? 1 : (int)$prize['pnum'];
            $id         = $prize['pid'];
            $_tmp_pool  = array_fill(0, $num, $id);
            $prize_pool = array_merge($prize_pool, $_tmp_pool);
        }
        shuffle($prize_pool);

        // 根据用户权重,生成候选用户池
        $user_pool = [];
        foreach ($users as $user) {
            $weight    = empty($user['weight']) ? 1 : (int)$user['weight'];
            $uid       = $user['uid'];
            $_tmp_pool = array_fill(0, $weight, $uid);
            $user_pool = array_merge($user_pool, $_tmp_pool);
        }
        shuffle($user_pool);

        // 随机出一个奖品
        $rand_prize_id = array_shift($prize_pool);
        // 从所有人员中,随机选出一个人跟前面的奖品进行关联
        $rand_user_id = array_shift($user_pool);
        $user_name    = $user_mapping[$rand_user_id];
        $prize_name   = $prize_mapping[$rand_prize_id];
        $data         = [
            'uid'        => $rand_user_id,
            'uname'      => $user_name['uname'],
            'userno'     => $user_name['userno'],
            'prize_id'   => $rand_prize_id,
            'prize_name' => $prize_name['pname'],
            'plevel' => $prize_name['plevel'],
        ];
        return $data;
    }

    /**
     * 生成随机测试数据
     *
     * @param $prizes
     * @param $users
     */
    private function _generateTestData(&$prizes, &$users) {
        if (empty($prizes)) {
            $prizes = [
                [
                    'pid'   => 1,
                    'pname' => '华为手机',
                    'pnum'  => 1,
                ],
                [
                    'pid'   => 2,
                    'pname' => '华为手表',
                    'pnum'  => 3,
                ],
                [
                    'pid'   => 3,
                    'pname' => '华为手绢',
                    'pnum'  => 5,
                ],
            ];
        }

        if (empty($users)) {
            $users = [
                [
                    'uid'    => 1,
                    'uname'  => 'wallas',
                    'weight' => 1,
                ],
                [
                    'uid'    => 2,
                    'uname'  => 'jerry',
                    'weight' => 20,
                ],
            ];
        }
    }
}
