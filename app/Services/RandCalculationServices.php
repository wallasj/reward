<?php

namespace App\Services;

//随机算法
use Illuminate\Support\Facades\Log;

class RandCalculationServices
{
    /**
     * @param $prizeArr     奖品列表
     * @param $chance       参与人数
     * @return mixed
     */
    public function reward(array $prizeArr, $chance)
    {
        $rate = [];
        $index = 0;
        //计算区间
        foreach ($prizeArr as $key => $item) {
            if ($key == 0) {
                $rate[$key] = [0, $item['num']];
            } else {
                $rate[$key] = [$rate[$key - 1][1], $rate[$key - 1][1] + $item['num']];
            }
            ++ $index;
        }
        Log::info('概率 ' . json_encode($rate));

        //出现异常
        if ($index == 0) return [];

        //判断数额是否已满了 - 人均机会大于1次的情况下
        if ($rate[$index - 1][1] < $chance && count($prizeArr) == 1) {
            $rate[$index - 1] = [$rate[$index - 1][0], $chance];
            Log::info('超高概率 ' . json_encode($rate));
        }

        //TODO 如果机会与奖品总数一样，则直接在奖品内随机
        if ($chance == count($prizeArr)) {
            Log::info('百分百概率');
            return $prizeArr[mt_rand(0, $chance - 1)];
        }

        $rd = mt_rand(0, $chance);
        Log::info('随机数 ' . json_encode($rd));
        foreach ($rate as $key => $item) {
            if ($item[0] <= $rd && $rd < $item[1]) {
                if (isset($prizeArr[$key])) {
                    return $prizeArr[$key];
                }
            }
        }

        return [
            'id' => 0,
            'name' => '很遗憾，未中奖，重在参与！',
            'num' => 999,
            'level' => 999
        ];
    }
}
