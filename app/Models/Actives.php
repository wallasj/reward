<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rewards
 * 活动表
 * @package App
 */
class Actives extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activeTitle', 'persons', 'actual_persons', 'description', 'timeDays',
        'timeHour', 'timeMinutes', 'beginTime', 'endTime', 'status', 'banners', 'chance'
    ];

    public static $_validRule = [
        'activeTitle' => 'required|max:50|unique:actives',
        'persons' => 'required|numeric',//|min:10
        'beginTime' => 'required|date',
        'endTime' => 'required|date',
        'status' => 'required|in:0,1',
        'chance' => 'required|numeric|max:2',
        'timeDays' => 'required',
        'timeHour' => 'required|numeric|max:24',
        'timeMinutes' => 'required|numeric|max:60'
    ];

    public static $_messages = [
        'activeTitle.required' => '请填写活动名称',
        'activeTitle.max' => '活动名称过长',
        'activeTitle.unique' => '活动名称请勿重复',

        'persons.required' => '请填写参与人数',
        'persons.numeric' => '参与人数为数字',
        //'persons.min' => '参与人数最少10人',

        'beginTime.required' => '请填写开始时间',
        'beginTime.date' => '开始时间格式错误',

        'endTime.required' => '请填写结束时间',
        'endTime.date' => '结束时间格式错误',

        'status.required' => '请选择奖品状态',
        'status.in' => '请选择奖品状态为【0下架 1上架】',

        'chance.required' => '抽奖次数',
        'chance.numeric' => '抽奖次数为数字',
        'chance.max' => '抽奖次数没人不得超过2次',

        'timeDays.required' => '请指定抽奖日期，按照“;”分割',
        'timeHour.required' => '请指定抽奖时间',
        'timeHour.numeric' => '抽奖时间为数字',
        'timeHour.max' => '小时不能超过24',

        'timeMinutes.required' => '请指定抽奖秒',
        'timeMinutes.numeric' => '抽奖时间为数字',
        'timeMinutes.max' => '秒不能超过60',
    ];

    public function prizes()
    {
        return $this->hasMany(ActivesPrizes::class, 'activiteId', 'id');
    }
}
