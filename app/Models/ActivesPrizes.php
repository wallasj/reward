<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rewards
 * 活动表
 * @package App
 */
class ActivesPrizes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activiteId', 'prizeId', 'quantity'
    ];

    public function prizeInfo()
    {
        return $this->hasOne(Prizes::class, 'id', 'prizeId');
    }
}
