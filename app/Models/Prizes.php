<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rewards
 * 奖品表
 * @package App
 */
class Prizes extends Model
{

    const level = [
        1 => '一等奖',
        2 => '二等奖',
        3 => '三等奖'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'imgs',
        'level', 'price', 'stock', 'status'
    ];

    public static $_validRule = [
        'name' => 'required|max:50|unique:prizes',
        'level' => 'required|numeric|max:255',
        'price' => 'numeric',
        'stock' => 'numeric',
        'status' => 'required|in:0,1'
    ];

    public static $_messages = [
        'name.required' => '请填写奖品名称',
        'name.max' => '奖品名称过长',
        'name.unique' => '奖品名称请勿重复',

        'level.required' => '请选择奖品等级',
        'level.numeric' => '等级为数字',
        'level.max' => '等级超过预期',

        'price.numeric' => '奖品金额为数字',

        'stock.numeric' => '奖品数量为数字',

        'status.required' => '请选择奖品状态',
        'status.in' => '请选择奖品状态为【0下架 1上架】',
    ];
}
