<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rewards
 * 参与名单
 * @package App
 */
class ParicipationList extends Model
{

    protected $table = 'paricipation_list';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activeId', 'userId', 'prizeId',
        'isReward', 'chance', 'status'
    ];

    public static $_validRule = [
        'activeId' => 'required',
        'userId' => 'required',
    ];

    public static $_messages = [
        'activeId.required' => '请填写活动ID',

        'userId.required' => '请填用户ID',
    ];

    public function active()
    {
        return $this->hasOne(Actives::class, 'id', 'activeId');
    }

    public function prize()
    {
        return $this->hasOne(Prizes::class, 'id', 'prizeId');
    }
}
