<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rewards
 * 中奖记录
 * @package App
 */
class WinnerRecord extends Model
{
    protected $table = 'winner_record';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activiteId', 'userId', 'deptId', 'prizeId',
        'name', 'level', 'status'
    ];

    public function activeInfo()
    {
        return $this->hasOne(Actives::class, 'id', 'activiteId');
    }

    public function prize()
    {
        return $this->hasOne(Prizes::class, 'id', 'prizeId');
    }
}
