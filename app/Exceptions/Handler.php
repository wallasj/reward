<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof AuthorizationException || $exception instanceof AuthenticationException) {
            return $this->_changeFormat($exception, '权限校验失败', '403');
        } else if ($exception instanceof ValidationException) {
            return $this->_changeFormat($exception, '参数错误', '400');
        } else if ($exception instanceof NotFoundHttpException || $exception instanceof  MethodNotAllowedHttpException) {
            return $this->_changeFormat($exception, '未找到该请求', '404');
        }
        return parent::render($request, $exception);
    }

    //修改返回类型
    private function _changeFormat(Exception $e, string $message, string $code)
    {
        $erros = '';
        if (method_exists($e, 'errors')) {
            $erros = $e->errors();
        } else if (method_exists($e, 'getMessage')) {
            $erros = $e->getMessage();
        }

        if ($code == '400' && is_array($erros)) {
            //每次只提示一条错误
            $erros = head($erros)[0];
        }
        return response()->json([
            'code' => $code,
            'message' => $message,
            'data' => $erros,
        ]);
    }
}
