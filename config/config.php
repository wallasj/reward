<?php

return [
    'user_api' => env('USERS_URL'),
    'user_secret' => env('USERS_SECRET'),
    'redis_prefix' => env('REDIS_PREFIX'),
    'page' => 1,
    'pageSize' => 15
];
